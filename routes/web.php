<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('admin.login.get');
});

Route::get('/admin', function(){
		return redirect()->route('admin.login.get');
	});

Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email.admin');

Route::get('reset/{token?}', 'Admin\Auth\ResetPasswordController@showResetForm')->name('password.reset.admin');

Route::post('password/reset', 'Admin\Auth\ResetPasswordController@reset')->name('admin.password.reset');

Route::get('admin/login', 'Admin\Auth\AdminLoginController@showLoginForm')->name('admin.login.get');

Route::post('admin/login-post', 'Admin\Auth\AdminLoginController@login')->name('admin.login.post');

Route::group(array('middleware' => ['admin']), function () {
	Route::get('/locked',array(
        'as'   => 'admin.locked',
        'uses' => 'LockController@getlocked'
    ));
	//route for dashboard
	Route::get('admin/dashboard', 'Admin\DashboardController@index')->name('admin.dashboard');
	//route for logout
	Route::get('admin/logout', 'Admin\Auth\AdminLoginController@logout')->name('admin.logout');
	
	Route::resource('admin/base_plate', 'Admin\BasePlateController',['except' => ['show']]);

	Route::resource('admin/beam', 'Admin\BeamController',['except' => ['show']]);

	Route::resource('admin/connector', 'Admin\ConnectorController',['except' => ['show']]);

	Route::resource('admin/bracing', 'Admin\BracingController',['except' => ['show']]);

	Route::get('admin/quotation','Admin\QuotationController@index')->name('quotation.index');
	Route::post('admin/quotation','Admin\QuotationController@store')->name('quotation.store');

	Route::get('admin/quotation/show','Admin\QuotationController@show')->name('quotation.show');
	Route::post('admin/quotation/show','Admin\QuotationController@generatePdf')->name('quotation.generate_pdf');

	Route::get('admin/myModalContent',function(){
		return view('admin.quotation.myModalContent');
	});
});

/*Route::get('admin/login',function(){
	return view('admin.login');
});
Route::get('admin/password-change',function(){
	return view('admin.email');
});
Route::get('admin/password-reset',function(){
	return view('admin.reset');
});
Route::get('admin/change-password',function(){
	return view('admin.changepassword');
});
Route::get('admin/dashboard',function(){
	return view('admin.dashboard');
});
Route::resource('admin/users', 'UserController',['except' => ['show']]);
Route::get('admin/dynamic-form',function(){
	return view('admin.dynamic-form.new');
});

Route::get('image/import',function(){
	return view('admin.image.index');
});
Route::get('admin/input',function(){
	return view('admin.input-type.index');
});
Route::get('/admin/input/form',function(){
	return view('admin.input-type.new');
});*/