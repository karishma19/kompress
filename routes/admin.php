<?php
// Route::get('routes',[
// 	'as' => 'show.routes',
// 	'uses'=>'Admin\UserController@RouteList'
// ]);
// Route::get('/access-denied',function(){
//             return view('admin.permission_denied');
//         });

// Route::group(array('namespace'=>'Admin','prefix' => 'admin'), function () {

// 	Route::get('/admin', function(){
// 		return redirect()->route('admin.login.get');
// 	});

// 	Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email.admin');

// 	Route::get('reset/{token?}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset.admin');

// 	Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('admin.password.reset');

// 	Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login.get');

// 	Route::post('/login-post', 'Auth\AdminLoginController@login')->name('admin.login.post');

// 	Route::group(['middleware' => ['admin']], function () {
// 		Route::get('/locked',array(
// 	        'as'   => 'admin.locked',
// 	        'uses' => 'LockController@getlocked'
// 	    ));
// 		//route for dashboard
// 		Route::get('/dashboard', 'DashboardController@index')->name('admin.dashboard');
// 		//route for logout
// 		Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
// 		//route for change-password
// 		Route::group(array('prefix' => 'user_masters','middleware' => ['admin','acl.permitted']), function () {
// 		Route::resource('roles','RoleController',['except'=>'destroy']);
// 		Route::get('role/status-change',[
// 					'as' => 'roles.status',
// 					'uses' => 'RoleController@changeStatus'
// 				]);
// 		Route::resource('users','UserController',['except'=>['destroy','show']]);
// 		Route::get('users/get-roles-name',[
// 					'as' => 'users.role.get.name',
// 					'uses' => 'UserController@getRoleName'
// 				]);
// 		Route::post('users/delete',[
// 			'as' => 'users.delete',
// 			'uses' => 'UserController@delete'
// 		]);
// 	});
// 	});
	
// });
