<?php

// for user
	Breadcrumbs::register('user', function ($breadcrumbs) {
	    $breadcrumbs->push('Users', route('users.index'), ['icon' => '<i class="fa fa-user"></i>']);
	});
	Breadcrumbs::register('create_user', function($breadcrumbs)
	{
		$breadcrumbs->parent('user');
		$breadcrumbs->push('Add Users', route('users.create'));
	});
	Breadcrumbs::register('edit_user', function($breadcrumbs,$user)
	{
		$breadcrumbs->parent('user');
		$breadcrumbs->push(substr($user->name,0,50), route('users.index',$user));
	});

	Breadcrumbs::register('role', function ($breadcrumbs) {
	    $breadcrumbs->push('Roles', route('roles.index'), ['icon' => '<i class="fa fa-user"></i>']);
	});
	Breadcrumbs::register('create_role', function($breadcrumbs)
	{
		$breadcrumbs->parent('role');
		$breadcrumbs->push('Add Roles', route('roles.create'));
	});
	Breadcrumbs::register('edit_role', function($breadcrumbs,$role_details)
	{
		$breadcrumbs->parent('role');
		$breadcrumbs->push(substr($role_details->name,0,50), route('roles.index',$role_details));
	});