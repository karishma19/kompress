<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Connector extends Model
{
    protected $table = 'connector';
    
    protected $fillable = ['leap','thickness','nett_weight','nett_weight_unit'];
}
