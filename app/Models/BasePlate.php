<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BasePlate extends Model
{
    // public $guard_name = 'admin';
    protected $table = 'base_plate';
    
    protected $fillable = ['height','weight','depth','thickness','nett_weight','nett_weight_unit'];
}
