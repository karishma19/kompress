<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DiagonalQty extends Model
{
    protected $table = 'diagonal_qty';
    
    protected $fillable = ['length','qty'];
}
