<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Beam extends Model
{
    protected $table = 'beam';
    
    protected $fillable = ['depth','height','min_thickness','max_thickness','min_length','max_length','material_type','depth_upto','load_bearing_capacity','lbc_unit','fold','nett_weight','nett_weight_unit'];
}
