<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BracingLength extends Model
{
    protected $table = 'bracing_length';
    
    protected $fillable = ['depth','horizonal_length','diagonal_length'];
}
