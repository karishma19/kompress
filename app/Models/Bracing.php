<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bracing extends Model
{
    protected $table = 'bracing';
    
    protected $fillable = ['depth','height','min_thickness','max_thickness','fold','material_type','nett_weight','nett_weight_unit'];
}
