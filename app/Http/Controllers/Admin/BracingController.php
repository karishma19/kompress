<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Bracing;
use App\Models\BracingLength;
use App\Models\DiagonalQty;

class BracingController extends Controller
{
    public function index(Request $request){
        $bracing = Bracing::all()->toArray();

        $bracing_length = BracingLength::all()->toArray();
        $diagonal_qty = DiagonalQty::all()->toArray();

        return view('admin.bracing.index',compact('bracing','bracing_length','diagonal_qty'));
    }
}
