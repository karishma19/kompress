<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\AclPermission;
use Illuminate\Support\Facades\Route;
use App\Helpers\Utility;
use App\Models\Admin;
use App\Models\Role;
use App\Models\RolePermission;
use App\Models\UserPermission;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use Mail;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
     {
        if ($request->ajax()) {

                $where_str = '1 = ?';
                $where_params = [1];
                
                 if ($request->has('sSearch')) {
                    $search = $request->get('sSearch');
                    $where_str .= " and ( admins.name like \"%{$search}%\""
                    ."or admins.email like \"%{$search}%\""
                    ."or roles.name like \"%{$search}%\""
                    . ")";
                }

                $columns = array('admins.id','admins.name','admins.email','roles.name as role_name','admins.status','admins.updated_at');
                $data_count = Admin::select('id')
                                    ->Leftjoin('roles','roles.id','=','admins.role')
                                    ->where('is_vendor','0')
                                    ->whereRaw($where_str, $where_params)
                                    ->count();

                $data = Admin::select($columns)
                            ->Leftjoin('roles','roles.id','=','admins.role')
                            ->where('is_vendor','0')
                            ->whereRaw($where_str, $where_params);

               

                if ($request->has('iDisplayStart') && $request->get('iDisplayLength') != '-1') {
                    $data = $data->take($request->get('iDisplayLength'))->skip($request->get('iDisplayStart'));
                
                }
                
                if ($request->has('iSortCol_0')) {
                    for ($i = 0; $i < $request->get('iSortingCols'); $i++) {
                        $column = $columns[$request->get('iSortCol_' . $i)];
                        if (false !== ($index = strpos($column, ' as '))) {
                            $column = substr($column, 0, $index);
                        }
                        $data = $data->orderBy($column, $request->get('sSortDir_' . $i));
                    }
                }
                
                $data = $data->get()->toArray();
                
                $response['iTotalDisplayRecords'] = $data_count;
                $response['iTotalRecords'] = $data_count;

                $response['sEcho'] = intval($request->get('sEcho'));

                $response['aaData'] = $data;


                return $response;
            }

            return view('admin.users.index');
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name', 'id');
            // $role_list = ['' => 'Select Role'] + $role_list;
            return view('admin.users.new', compact('roles'));
    }
     public function store(UserRequest $request)
    {
        $name = $request->get('name');
        $email = $request->get('email');
        $role = $request->get('roles');
        $status = $request->get('status');
        $role_permission = $request->get('permission');
        
            $generate_password = Utility::getRandomCharString(8); 
            $password = bcrypt($generate_password);
            $user_save = new Admin();
            $user_save->name = $name;
            $user_save->email = $email;
            $user_save->password = $password;
            $user_save->role = $role;
            $user_save->status = $status;
            // dd($user_save);
            $user_save->save();
            $user_id = $user_save->id;
            // $current_permission_delete = UserPermissions::where('role_id',$role_id)->delete();
            $created_at = auth('admin')->user()->id;
            foreach ($role_permission as $key => $single_permission) {
                // print_r($single_permission);
                $route_name = AclPermission::select('route_name')->where('id',$single_permission)->get()->toArray();
                
                // echo "<pre>";print_r($route_name[0]['route_name']);
                // exit;
                $role_permission_save = new UserPermission();
                // echo "<pre>";
                // print_r($route_name);
                $role_permission_save->user_id = $user_id;
                $role_permission_save->permission_id = $single_permission;
                $role_permission_save->route_name = $route_name[0]['route_name'];
                $role_permission_save->created_by = $created_at;
                $role_permission_save->save();
            }
            // dd($user_save);
            // $subject = 'Your Login Credentials for Gizmojo';
            // $email_view = 'admin.send_password';
            Mail::send('admin.send_password', ['data' => $user_save,'password'=>$generate_password], function ($message) use ($user_save) {
                            $message->to('mk@gmail.com')->subject('Just one step to go');
                        });
            // $user = Admin::create($request->all());
            // $send_email_user = $this->dispatch(new SendMailJob($email_view,['name'=>$user_save->name,'email'=>$user_save->email,'password'=>$generate_password],$user_save->email,$subject));

            //  if ($request->save_button == 'save_new') {
            //     return back()->with('message', 'User added successfully')
            //         ->with('message_type', 'success');
            // }
            // return redirect()->route('users.index')
            //                 ->with('message','User Added successfully')
            //                 ->with('message_type','success');
            return response()->json(array('success' => true,'action'=>'added'),200);
    }

    public function getRoleName(Request $request){
        $id = $request->id;
       
            $role_current_permissions = RolePermission::where('role_id',$id)->pluck('permission_id')->toArray();
            $user_permission = AclPermission::getPermission();
            // dd($role_current_permissions);
            $get_html = view('admin.users.role', compact('role_current_permissions','user_permission'))->render();

            return response()->json(array('html'=>$get_html,'success' => true), 200);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $user = Admin::findOrFail($id);
            $user_permission = AclPermission::getPermission();
            $roles = Role::pluck('name','id');
            // dd($user_permission);
            $role_current_permissions = UserPermission::where('user_id',$id)->pluck('permission_id')->toArray();

            return view('admin.users.edit',compact('user','roles','user_permission','role_current_permissions','id'));
    }
    public function show($id)
    {
       
    }
    public function update(Request $request)
    {
       $name = $request->get('name');
            $email = $request->get('email');
            $role = $request->get('roles');
            $status = $request->get('status');
            $role_permission = $request->get('permission');
            
            $user_save = Admin::findOrFail($request->id);
            $user_save->name = $name;
            $user_save->email = $email;
            $user_save->role = $role;
            $user_save->status = $status;

            $user_save->save();
            $user_id = $user_save->id;
            // dd($user_id);
            $current_permission_delete = UserPermission::where('user_id',$user_id)->delete();

            $created_at = auth('admin')->user()->id;
            foreach ($role_permission as $key => $single_permission) {
                // print_r($single_permission);
                $route_name = AclPermission::select('route_name')->where('id',$single_permission)->get()->toArray();
                
                // echo "<pre>";print_r($route_name[0]['route_name']);
                // exit;
                $role_permission_save = new UserPermission();
                // echo "<pre>";
                // print_r($route_name);
                $role_permission_save->user_id = $user_id;
                $role_permission_save->permission_id = $single_permission;
                $role_permission_save->route_name = $route_name[0]['route_name'];
                $role_permission_save->created_by = $created_at;
                $role_permission_save->updated_by = $created_at;
                $role_permission_save->save();
            }
            //dd($user);
            // $this->syncPermissions($request, $user);
            // $user->save();
             // return redirect()->route('users.index')
             //                ->with('message','User updated successfully')
             //                ->with('message_type','success');

            return response()->json(array('success' => true,'action'=>'updated'),200);
    }
    public function RouteList(){
            // $dd= Route::getCurrentRoute()->getActionName();
            $routes = Route::getRoutes();
            
            $route_array = [];
            foreach ($routes as $value) {
                $route = $value->getName();
                $log_viewer = explode('::', $route);
                
                $route_method = $value->methods[0]; //method
                $route_action = $value->getActionName(); //method
                $route_path = $value->uri();  //url
                $route_inner = explode('.', $route);
                $last_index = last($route_inner);
                // dd($route_inner);
                if ($route != '') {
                    $route_title = $route_inner[0];
                    $route_save = AclPermission::firstOrNew(['route_name'=>$route]);
                    $route_save->route_name = $route;
                    $route_save->main_module = head($route_inner);
                    $route_save->sub_module = $route_title;
                    $route_save->method_name = $route_method;
                    $route_save->module = $last_index;
                    $route_save->action_name = $route_action;
                    $route_save->route_method = $last_index;

                    if ($last_index == 'index') {
                        $route_save->description = 'Listing of '.$route_title;
                    }elseif($last_index == 'create'){
                        $route_save->description = 'Create page of '.$route_title;
                    }elseif($last_index == 'edit'){
                        $route_save->description = 'Edit page of '.$route_title;
                    }elseif($last_index == 'delete'){
                        $route_save->description = 'Delete '.$route_title;
                    }else{
                        $route_save->description =  ucfirst($last_index).' of '.$route_title;
                    }
                    if($log_viewer[0] != 'log-viewer'){
                        $route_save->save();
                    }
                }
                $route_array[] = [
                    'route' => $route,
                    'route_method' => $route_method,
                    'route_action' => $route_action,
                    'route_path' => $route_path
                ];
            }

            echo "<pre>";
            echo count($route_array);
            print_r($route_array);
            exit();
        
    }

    public function delete(Request $request){

        $id = $request->get('id');
        if (!is_array($id)) {
            $id = array($id);
        }
        $student_delete = Admin::whereIn('id', $id)->delete();
        $permission_delete = UserPermission::where('user_id',$id)->delete();
        return response()->json(array('success' => true), 200);
    }
}
