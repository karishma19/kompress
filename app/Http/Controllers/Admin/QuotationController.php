<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\BasePlate;
use App\Models\Beam;
use App\Models\Bracing;
use App\Models\BracingLength;
use App\Models\Connector;
use App\Models\DiagonalQty;

class QuotationController extends Controller
{
    public function index(){

    	$total_product = [];

    	$total_product['base_plate'] = BasePlate::get()->toArray();
    	$beam = Beam::orderBy('depth_upto','asc')->get()->toArray();
    		
    	foreach ($beam as $key => $single_beam) {
    		$total_product['beam'][$single_beam['depth_upto']][] = $single_beam;
    		sort($total_product['beam'][$single_beam['depth_upto']]);
    	}
    	
    	$total_product['bracing'] = Bracing::get()->toArray();
    	$total_product['bracing_length'] = BracingLength::get()->toArray();
    	$total_product['connector'] = Connector::get()->toArray();
    	$total_product['diagonal_qty'] = DiagonalQty::get()->toArray();

    	return view('admin.quotation.index',compact('total_product'));
    }

    public function store(Request $request){
        $total_quotation = $request->get('total_quotation');
        $total_quotation = json_decode($total_quotation,true);
   
        $request->session()->put('total_quotation',$total_quotation);

        return redirect()->route('quotation.show');
    }

    public function show(Request $request){
        $details = $request->session()->get('total_quotation');
     
        return view('admin.quotation.show',compact('details'));
    }

    public function generatePdf(Request $request){
        $storage = $request->get('storage');
        $pallet = $request->get('pallet');
        $details = $request->session()->get('total_quotation');

        $mpdf = new \Mpdf\Mpdf(['tempDir' => public_path()]);
        $header = "<img src='".public_path() .'/backend/images/1.jpg'."' style='height:80px;'/>";
        $footer = "hello";
        $mpdf->SetHeader($header,'O');
        $mpdf->SetFooter($footer);
        $mpdf->AddPage('', // L - landscape, P - portrait 
        '', '', '', '',
                        20, // margin_left
                        20, // margin right
                       40, // margin top
                       20, // margin bottom
                        10, // margin header
                        3); // margin footer
        $mpdf->WriteHTML(\View::make('admin.quotation.pdf',compact('details','pallet','storage'))->render());
        $filename = sha1(microtime())."_".'pdfview.pdf';
        $mpdf->output(public_path()."/".$filename);
        return response()->download(public_path()."/".$filename);
    }
}