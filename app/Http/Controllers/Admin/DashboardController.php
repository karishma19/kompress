<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FitnessBranch;
use App\Models\FitnessCenter;
use App\Models\Trainer;
use App\Models\Customer;

class DashboardController extends Controller{
	/**
	 * Display a Dashboard page.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
    public function index()
    {
        return view('admin.dashboard');
    }
}