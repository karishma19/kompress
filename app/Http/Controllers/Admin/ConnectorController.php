<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Connector;

class ConnectorController extends Controller
{
    public function index(Request $request){
    	if ($request->ajax()) {
            $where_str = "1 = ?";
            $where_params = array(1);

            if (!empty($request->input('sSearch'))) {
                $search = $request->input('sSearch');
                $where_str .= " and ( leap like \"%{$search}%\""
                	. " or thickness like \"%{$search}%\""
                	. " or nett_weight like \"%{$search}%\""
                	. " or nett_weight_unit like \"%{$search}%\""
                    . ")";
            }
            $columns = array('id','leap','thickness','nett_weight','nett_weight_unit');


            $connector = Connector::select($columns)
                ->whereRaw($where_str, $where_params);  
                
            $connector_count = Connector::select('id','leap','thickness','nett_weight','nett_weight_unit')
                ->whereRaw($where_str, $where_params)
                ->count();

            if ($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != '') {
                $connector = $connector->take($request->input('iDisplayLength'))
                    ->skip($request->input('iDisplayStart'));
            }
            if ($request->input('iSortCol_0')) {
                $sql_order = '';
                for ($i = 0; $i < $request->input('iSortingCols'); $i++) {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $connector = $connector->orderBy($column, $request->input('sSortDir_' . $i));
                }
            }
            $connector = $connector->get();

            $response['iTotalDisplayRecords'] = $connector_count;
            $response['iTotalRecords'] = $connector_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $connector->toArray();

            return $response;
        }
        return view('admin.connector.index');
    }
}
