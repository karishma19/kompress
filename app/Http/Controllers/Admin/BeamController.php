<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Beam;

class BeamController extends Controller
{
    public function index(Request $request){
    	if ($request->ajax()) {
            $where_str = "1 = ?";
            $where_params = array(1);

            if (!empty($request->input('sSearch'))) {
                $search = $request->input('sSearch');
                $where_str .= " and ( depth like \"%{$search}%\""
                	. " or height like \"%{$search}%\""
                	. " or min_thickness like \"%{$search}%\""
                	. " or max_thickness like \"%{$search}%\""
                	. " or min_length like \"%{$search}%\""
                    . " or max_length like \"%{$search}%\""
                    . " or material_type like \"%{$search}%\""
                    . " or depth_upto like \"%{$search}%\""
                    . " or load_bearing_capacity like \"%{$search}%\""
                    . " or lbc_unit like \"%{$search}%\""
                    . " or fold like \"%{$search}%\""
                    . " or nett_weight like \"%{$search}%\""
                	. " or nett_weight_unit like \"%{$search}%\""
                    . ")";
            }
            $columns = array('id','depth','height','min_thickness','max_thickness','min_length','max_length','material_type','depth_upto','load_bearing_capacity','lbc_unit','fold','nett_weight','nett_weight_unit');


            $beam = Beam::select($columns)
                ->whereRaw($where_str, $where_params);  
                
            $beam_count = Beam::select('id','depth','height','min_thickness','max_thickness','min_length','max_length','material_type','depth_upto','load_bearing_capacity','lbc_unit','fold','nett_weight','nett_weight_unit')
                ->whereRaw($where_str, $where_params)
                ->count();

            if ($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != '') {
                $beam = $beam->take($request->input('iDisplayLength'))
                    ->skip($request->input('iDisplayStart'));
            }
            if ($request->input('iSortCol_0')) {
                $sql_order = '';
                for ($i = 0; $i < $request->input('iSortingCols'); $i++) {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $beam = $beam->orderBy($column, $request->input('sSortDir_' . $i));
                }
            }
            $beam = $beam->get();

            $response['iTotalDisplayRecords'] = $beam_count;
            $response['iTotalRecords'] = $beam_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $beam->toArray();

            return $response;
        }
        return view('admin.beam.index');
    }
}
