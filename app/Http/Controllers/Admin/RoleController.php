<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\AclPermission;
use App\Models\RolePermission;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
                $where_str = "1 = ?";
                $where_params = array(1);

                if (!empty($request->input('sSearch'))) {
                    $search = $request->input('sSearch');
                    $where_str .= " and ( name like \"%{$search}%\""
                    . ")";
                }
                $columns = ['id', 'name','status','updated_at'];


                $role = Role::select($columns)       
                            ->whereRaw($where_str, $where_params);

                $role_count = Role::select('id')
                            ->whereRaw($where_str, $where_params)
                            ->count();

                if ($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != '') {
                    $role = $role->take($request->input('iDisplayLength'))
                        ->skip($request->input('iDisplayStart'));
                }
                if ($request->input('iSortCol_0')) {
                    $sql_order = '';
                    for ($i = 0; $i < $request->input('iSortingCols'); $i++) {
                        $column = $columns[$request->input('iSortCol_' . $i)];
                        if (false !== ($index = strpos($column, ' as '))) {
                            $column = substr($column, 0, $index);
                        }
                        $role = $role->orderBy($column, $request->input('sSortDir_' . $i));
                    }
                }
                $role = $role->get();

                $response['iTotalDisplayRecords'] = $role_count;
                $response['iTotalRecords'] = $role_count;
                $response['sEcho'] = intval($request->input('sEcho'));
                $response['aaData'] = $role->toArray();

                return $response;
            }
            return view('admin.role.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $user_permission = AclPermission::getPermission();
            // dd($user_permission);
            return view('admin.role.create',compact('user_permission'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
        ]);
        $role_name = $request->get('name');
        $role_permission = $request->get('permission');

        $id = auth('admin')->user()->id;
        $role_save = new Role();
            $role_save->name = $role_name;
            $role_save->description = $request->get('description');
            $role_save->created_by = $id;
            $role_save->updated_by = $id;
            $role_save->save();

            $role_id  = $role_save->id;
            if(isset($role_permission)){
                foreach ($role_permission as $key => $single_permission) {
                    $role_permission_save = new RolePermission();

                    $role_permission_save->role_id = $role_id;
                    $role_permission_save->permission_id = $single_permission;
                    $role_permission_save->created_by = $id;
                    $role_permission_save->updated_by = $id;
                    $role_permission_save->save();
                }
            }

        return response()->json(array('success' => true,'action'=>'added'),200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_permission = AclPermission::getPermission();
                
        $role_details = Role::findOrFail($id);
        $role_current_permissions = RolePermission::where('role_id',$id)->pluck('permission_id')->toArray();
        // dd($role_current_permissions);

        return view('admin.role.edit',compact('user_permission','role_details','role_current_permissions','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->id;
        $role_name = $request->get('name');
        $role_permission = $request->get('permission');

        $user_id = auth('admin')->user()->id;
        $role_edit_save = Role::findOrFail($request->id);
            $role_edit_save->name = $role_name;
            $role_edit_save->description = $request->get('description');
            $role_edit_save->updated_by = $user_id;
            $role_edit_save->save();

            $role_id  = $role_edit_save->id;

            $current_permission_delete = RolePermission::where('role_id',$role_id)->delete();

            foreach ($role_permission as $key => $single_permission) {
                $role_permission_save = new RolePermission();

                $role_permission_save->role_id = $role_id;
                $role_permission_save->permission_id = $single_permission;
                $role_permission_save->created_by = $user_id;
                $role_permission_save->updated_by = $user_id;
                $role_permission_save->save();
            }
            return response()->json(array('success' => true,'action'=>'updated'),200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function changeStatus(Request $request){
        $id = $request->id;

        $activeStatus = $request->status;
        if ($activeStatus == 0) {
            echo $updateRecord = Role::where('id', $id)->update(['status' => '1']);
        }
        if ($activeStatus == 1) {
            echo $updateRecord = Role::where('id', $id)->update(['status' => '0']);
        }
    }
}
