<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BasePlate;

class BasePlateController extends Controller
{
    public function index(Request $request){
    	if ($request->ajax()) {
            $where_str = "1 = ?";
            $where_params = array(1);

            if (!empty($request->input('sSearch'))) {
                $search = $request->input('sSearch');
                $where_str .= " and ( height like \"%{$search}%\""
                	. " or weight like \"%{$search}%\""
                	. " or depth like \"%{$search}%\""
                	. " or thickness like \"%{$search}%\""
                	. " or nett_weight like \"%{$search}%\""
                	. " or nett_weight_unit like \"%{$search}%\""
                    . ")";
            }
            $columns = array('id','height','weight','depth','thickness','nett_weight','nett_weight_unit');


            $base_plate = BasePlate::select($columns)
                ->whereRaw($where_str, $where_params);  
                
            $base_plate_count = BasePlate::select('id','height','weight','depth','thickness','nett_weight','nett_weight_unit')
                ->whereRaw($where_str, $where_params)
                ->count();

            if ($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != '') {
                $base_plate = $base_plate->take($request->input('iDisplayLength'))
                    ->skip($request->input('iDisplayStart'));
            }
            if ($request->input('iSortCol_0')) {
                $sql_order = '';
                for ($i = 0; $i < $request->input('iSortingCols'); $i++) {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if (false !== ($index = strpos($column, ' as '))) {
                        $column = substr($column, 0, $index);
                    }
                    $base_plate = $base_plate->orderBy($column, $request->input('sSortDir_' . $i));
                }
            }
            $base_plate = $base_plate->get();

            $response['iTotalDisplayRecords'] = $base_plate_count;
            $response['iTotalRecords'] = $base_plate_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $base_plate->toArray();

            return $response;
        }
        return view('admin.base_plate.index');
    }
}
