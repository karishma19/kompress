<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class LockController extends Controller
{
    public function getlocked(Request $request){
        if (Auth::guard('admin')->check()) {
            $user = Auth::guard('admin')->user();
            Auth::guard('admin')->logout();
            return View('admin.locked',compact('user'));
        }else{
            return redirect('/admin/login');
        }        
    }
}
