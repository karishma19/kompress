<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       switch ($this->method()) {
        case 'POST':
            {
                $rules= [
                    'name' => 'required|unique:roles,name',
                    'permission' => 'required'
                ];
                return $rules;
            }
        case 'PATCH':
            {
                $data = $this->all();

                $rules= [
                    'name' => 'required|unique:roles',.$data['name'],
                    'permission' => 'required'
                ];
                return $rules;
            }
        default:break;
        }
    }
    public function messages() {
        return [
            'name.required' => 'Please enter role name',
            'permission.required' => 'Please select permission'
        ];
    }
}
