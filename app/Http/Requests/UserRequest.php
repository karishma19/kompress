<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
        case 'POST':
            {
                $rules= [
                    'name' => 'required',
                    'email' => 'required|email|unique:admins,email',
                    'roles' => 'required'
                ];
                return $rules;
            }
        case 'PATCH':
            {
                $data = $this->all();
                // print_r($data);
                $rules= [
                    'name' => 'required',
                    'email' => 'required|email|unique:admins,email,'.$data['id'],
                    'roles' => 'required'

                ];
                return $rules;
            }
        default:break;
        }
    }
    public function messages() {
        return [
            'name.required' => 'Please enter name',
            'roles.required' => 'Please select role name'
        ];
    }
}
