<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin</title>
    
    <?= Html::style('backend/css/main.css',[],IS_SECURE) ?>

</head>
<body class="lock-screen" onload="startTime()">
    <div class="cover"></div>
    <div class="lock-wrapper">
        <div class="lock-body">
            <div class="lock-box text-center">
                <div style="font-size: 300%;color:#fff">GIZMOJO</div>
                <div id="time" style="font-size: 200%;"></div>
                <h1>{{ $user->name }}</h1>
                <span class="locked">Locked</span>
                    <?= Form::open(['url'=>route('admin.login.post'),'class'=>'lockscreen-credentials']) ?>
                    <input type="hidden" name="email" class="form-control" value="{{ $user['email'] }}" >
                    <div class="form-group col-lg-12">
                        <input type="password" placeholder="Password" name="password" class="form-control lock-input" required>
                        <button class="btn btn-lock" type="submit">
                            <i class="fa fa-arrow-right"></i>
                        </button>
                    </div>
                    <?= Form::close() ?>
            </div>
        </div>
    </div>
<?= Html::script('backend/js/jquery-2.1.4.min.js')?>
<?= Html::script('backend/js/bootstrap.min.js',[],IS_SECURE) ?>
<?= Html::script('backend/js/essential-plugins.js')?>
<?= Html::script('backend/js/main.js',[],IS_SECURE) ?>
    <script>
        function startTime()
        {
            var today=new Date();
            var d = today.getDate();
            var month = today.getMonth();
            var y = today.getFullYear();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            // add a zero in front of numbers<10
            m=checkTime(m);
            s=checkTime(s);
            // d+"-"+month+"-"+y+":"+
            document.getElementById('time').innerHTML=  h+":"+m+":"+s;
            t=setTimeout(function(){startTime()},500);
        }

        function checkTime(i)
        {
            if (i<10)
            {
                i="0" + i;
            }
            return i;
        }
    </script>
</body>
</html>