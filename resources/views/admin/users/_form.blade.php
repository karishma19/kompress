<div class="relative">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="panel-heading">
                    User Details
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('name')) {{ 'has-error' }} @endif">
                                    <div class="control-label col-md-4">Name<sup class="text-danger">*</sup>
                                    </div>
                                    <div class="col-md-8">
                                        <?=Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']);?>
                                        <span id="name_error" class="help-inline text-danger"><?=$errors->first('name')?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('email')) {{ 'has-error' }} @endif">
                                    <div class="control-label col-md-4">Email<sup class="text-danger">*</sup></div>
                                    <div class="col-md-8">
                                        <?=Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email']);?>
                                        <span id="email_error" class="help-inline text-danger"><?=$errors->first('email')?></span>
                                    </div>
                                </div>
                            </div>
                    
                            <div class="col-md-6">
                                <!-- Roles Form Input -->                     
                                <div class="form-group @if ($errors->has('roles')) has-error @endif">
                                    <div class="control-label col-md-4">Roles<sup class="text-danger">*</sup></div>
                                    <div class="col-md-8">
                                        <?= Form::select('roles',$roles,isset($user) ? $user->role : null,  ['class' => 'form-control select2','placeholder'=>'Select Role','id'=>'role' ]); ?>
                                       <span id="roles_error" class="help-inline text-danger"><?=$errors->first('roles')?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('address')) {{ 'has-error' }} @endif">
                                    <div class="control-label col-md-4">Status</div>
                                    <div class="col-md-8">
                                        <div class="animated-radio-button pull-left mr-10">
                                            <label class="control-label" for="is_active_true">
                                                <?=Form::radio('status', 1, true + old('status'), ['id' => 'is_active_true'])?>
                                                <span class="label-text"></span> Active
                                            </label>
                                        </div>
                                        <div class="animated-radio-button pull-left">
                                            <label class="control-label" for="is_active_false">
                                                <?=Form::radio('status', 0, old('status'), ['id' => 'is_active_false'])?>
                                                <span class="label-text"></span> Deactive
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="clearix"></div>
        </div>
    </div>
</div>