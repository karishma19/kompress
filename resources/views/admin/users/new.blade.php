@extends('admin.layout.layout')
@section('style')
    <?= Html::style('backend/plugins/jquery-multi-select/css/multi-select.css',[],IS_SECURE) ?>
@stop
@section('start_form')
    <form id="user_create_form" class="m-0 form-horizontal">
@stop
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
    <div class="title">
        <h4><?=Breadcrumbs::render('create_user') ?></h4>
    </div>
    <div class="pl-10">
        <button type="submit" name="save_button" value="save_new" id="save_new_1" class="btn btn-primary btn-sm disabled-btn save" title="Save and add new">Save & Add New</button>
        <button type="submit" name="save_button" value="save_exit" id="save_exit_1" class="btn btn-primary btn-sm disabled-btn save" title="Save">Save</button>
        <a href="<?=URL::route('users.index')?>" class="btn btn-default btn-sm" title="Back to users Page">Cancel</a>
    </div>
</nav>
@stop
@section('content')
@include('admin.users._form')

<div id="permission_add" class="row"></div>

<div class="text-right">
   <button type="submit" name="save_button" value="save_new" id="save_new_2" class="btn btn-primary btn-sm disabled-btn save" title="Save & Add New user">Save & Add New</button>
    <button type="submit" name="save_button" value="save_exit" id="save_exit_2" class="btn btn-primary btn-sm disabled-btn save" title="Save">Save</button>
    <a href="<?=URL::route('users.index')?>" class="btn btn-default btn-sm" title="Back to user Page">Cancel</a>
</div>
@stop

@section('end_form')
</form>
@stop
@section('script')
 <?= Html::script('backend/js/jquery.form.min.js',[],IS_SECURE) ?>
  <?= Html::script('backend/plugins/jquery-multi-select/js/jquery.multi-select.js',[],IS_SECURE) ?>
    <?= Html::script('backend/js/select2.min.js',[],IS_SECURE) ?>
    <script type="text/javascript">
        $('.user_permission_multi').multiSelect();
        $('.user_permission_multi').multiSelect('deselect_all');

        $('form').submit(function(){
            $('.overlay').show();
        });

        $("#selectall").click(function(){
            var is_checked = $(this).is(':checked');
            $(".sub_selection").prop('checked',is_checked);
            if (is_checked == true) {
                $('.user_permission_multi').multiSelect('select_all');
            }else{
                $('.user_permission_multi').multiSelect('deselect_all');
            }
        });

        $('.sub_selection').click(function(){
            var is_checked = $(this).is(':checked');
            var sub_selection_value = $(this).val();

            if (is_checked == true) {
                $('.'+sub_selection_value).multiSelect('select_all');
            }else{
                $('.'+sub_selection_value).multiSelect('deselect_all');
            }
        })

        $("input[type='checkbox'][class*='select_']").click(function(){
            var getClass = $(this).attr('class');
            var getSection = getClass.split(' ');
            var getSection = getSection[1].slice(7);
            var is_checked = $(this).is(':checked');
            $("."+getSection).prop('checked',is_checked);
        });
    </script>
    <script type="text/javascript">
        $('.select2').select2();
        
        // user create
        $(".save").click(function(e){
            e.preventDefault();
            var val = $(this).val();
            var url = "<?= URL::route('users.store') ?>";
            var method_type = 'POST';
            var token = "<?=csrf_token()?>";
        
            $('#user_create_form').ajaxSubmit({
                url: url,
                type: method_type,
                data: { "_token" : token },
                dataType: 'json',
                
                beforeSubmit : function()
                {
                    if(val == 'save_new')
                    {
                        $('#save_new_1').attr('disabled',true);
                        $('#save_new_1').html('<i class="fa fa-spinner fa-spin"></i> Please wait...');
                        $('#save_new_2').attr('disabled',true);
                        $('#save_new_2').html('<i class="fa fa-spinner fa-spin"></i> Please wait...');
                    }
                    else
                    {
                        $('#save_exit_1').attr('disabled',true);
                        $('#save_exit_1').html('<i class="fa fa-spinner fa-spin"></i> Please wait...');
                        $('#save_exit_2').attr('disabled',true);
                        $('#save_exit_2').html('<i class="fa fa-spinner fa-spin"></i> Please wait...');
                    }
                    $("[id$='_error']").empty();
                },
                
                success : function(resp)
                {             
                console.log(val);     
                    $(".overlay").hide();
                    
                    if (resp.success == true) {
                        var  action = resp.action;
                        toastr.success('User successfully '+action);

                        if(val == 'save_new')
                        {
                            $('#save_new_1').attr('disabled',false);
                            $('#save_new_1').html('Save & Add New');
                            $('#save_new_2').attr('disabled',false);
                            $('#save_new_2').html('Save & Add New');
                            window.location.href = "<?=route('users.create')?>";
                        }
                        else
                        {
                            $('#save_exit_1').attr('disabled',false);
                            $('#save_exit_1').html('Save');
                            $('#save_exit_2').attr('disabled',false);
                            $('#save_exit_2').html('Save');
                            window.location.href = "<?=route('users.index')?>";
                        }
                    }
                   
                },
                error : function(respObj){    

                    $.each(respObj.responseJSON.errors, function(k,v){
                        $('#'+k+'_error').text(v);
                    });
                    toastr.error('there were some errors!');
                    if(val == 'save_new')
                    {
                        $('#save_new_1').attr('disabled',false);
                        $('#save_new_1').html('Save & Add New');
                        $('#save_new_2').attr('disabled',false);
                        $('#save_new_2').html('Save & Add New');
                    }
                    else
                    {
                        $('#save_exit_1').attr('disabled',false);
                        $('#save_exit_1').html('Save');
                        $('#save_exit_2').attr('disabled',false);
                        $('#save_exit_2').html('Save');
                    }
                    $(".overlay").hide();
                }
            });
        });

        $('#role').change(function(){
            var val = $(this).val();
            var url = "<?= URL::route('users.role.get.name')?>";
            var method_type = 'GET';
            var token = "<?=csrf_token()?>";
            console.log(url);
            $.ajax({
                type:method_type,
                url:url,
                data:{'id': val,'token': token},
                dataType:'json',
                success: function(respObj) {
                    if(respObj.success == true)
                    {
                        $('#permission_add').html(respObj.html);
                    }
                }
            });
        });

    </script>
    @include('admin.layout.alert')
@endsection
