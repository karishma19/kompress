@extends('admin.layout.layout')
@section('style')
    <?=Html::style('backend/plugins/sweetalert/sweetalert.min.css', [], IS_SECURE)?>
    <?= Html::style('backend/css/dataranger.css',[],IS_SECURE) ?>
    <style type="text/css">
        .box__dragndrop,
        .box__uploading,
        .box__success,
        .box__error {
            display: none;
        }
    </style>
@stop
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
    <div class="title">
        <h4><?=Breadcrumbs::render('user') ?></h4>
    </div>
    <div class="top_filter"></div>
    <div class="pl-10">
          <a href="<?= route('users.create') ?>" class="btn btn-primary btn-sm" title="Add user" data-toggle="modal">Add USERS</a>
        
    </div>
</nav>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card p-0">
            <div class="card-body table-responsive">
               <!--  <ul class="nav nav-tabs">
                    <li class="@if(Request::get('type') == 'active') active @endif">
                        <a href="<?=route('users.index',['type'=>'active'])?>">Active</a>
                    </li>
                    <li  class="@if(Request::get('type') == 'in-active') active @endif">
                        <a href="<?=route('users.index',['type'=>'in-active'])?>">Inactive</a>
                    </li>
                </ul> -->
                <div class="text-right">
                    <div class="number-delete">
                        <ul>
                            <li>
                                <p class="mb-0"><span class="num_item"></span>Item Selected.</p>
                            </li>
                            <li class="bulk-dropdown"><a href="javascript:;">Bulk actions<span class="caret"></span></a>
                                <div class="bulk-box">
                                    <div class="bulk-tooltip"></div>
                                    <ul class="bulk-list">
                                        <li><a href="javascript:void(0);" id="delete" class="delete-btn">Delete selected user</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>                
                <div class="p-20">
                    <table class="table" id="user">
                        <thead>
                            <tr>
                                <th class="select-all no-sort">
                                    <!-- <div class="animated-checkbox">
                                        <label class="m-0">
                                            <input type="checkbox" id="checkAll"/>
                                            <span class="label-text"></span>
                                        </label>
                                    </div> -->
                                </th>
                                <th>Name Of users</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Active</th>
                                <th></th>
                                <th>Action</th>
                                
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            @include('admin.layout.overlay')
        </div>
    </div>
</div>
@stop
<!-- for error list show in model -->
@section('script')

<?=Html::script('backend/plugins/datatable/jquery.dataTables.min.js', [], IS_SECURE)?>
<?=Html::script('backend/plugins/datatable/dataTables.bootstrap.min.js', [], IS_SECURE)?>
<?=Html::script('backend/plugins/sweetalert/sweetalert.min.js', [], IS_SECURE)?>
<?=Html::script('backend/js/fnstandrow.js', [], IS_SECURE)?>
<?=Html::script('backend/js/delete_script.js', [], IS_SECURE)?>
<?= Html::script('backend/js/select2.min.js',[],IS_SECURE) ?>
<?= Html::script('backend/js/moment.min.js',[],IS_SECURE) ?>
<?= Html::script('backend/js/dateranger.js',[],IS_SECURE) ?>

<script type="text/javascript">
var table = "user";
var title = "Are you sure to delete this User?";
var text = "You will not be able to recover this record";
var type = "warning";
var delete_path = "<?= route('users.delete') ?>";
var token = "<?=csrf_token()?>";
var page_type = "<?=Request::get('type')?>";
var page_url = "<?=route('users.index')?>";

$(function(){
    $('#delete').click(function(){
        var delete_id = $('#'+table+' tbody .checkbox:checked');
        checkLength(delete_id);
    });

    $('#user').DataTable({
        "bProcessing": false,
        "bServerSide": true,
        "autoWidth": false,
        "sAjaxSource": page_url,
        "aaSorting": [ 5,"desc"],
        "aoColumns": [
            {   
                mData: "id",
                bSortable:false,
                bVisible:false,
                sWidth:"2%",
                sClass:"text-center",
                mRender: function (v, t, o) {
                    return '<div class="animated-checkbox"><label class="m-0"><input class="checkbox" type="checkbox" id="chk_'+v+'" name="special_id['+v+']" value="'+v+'"/><span class="label-text"></span></label></div>';
                },
            },
            
            {   mData:"name",sWidth:"20%",bSortable : true,
                    // mRender: function(v, t, o) {
                    //         var edit_path = "<?= URL::route('users.edit',array(':id')) ?>";
                    //         edit_path = edit_path.replace(':id',o['id']);

                    //         var act_html  = '<a title="Edit role" href="'+edit_path+'">'+ v +'</a>'
                                            
                    //         return act_html;
                    //     }
            },
            {   
                mData:"email",bSortable : true,sClass : 'text-left',sWidth:"30%",
            },
            {   
                mData:"role_name",bSortable : false,sClass : 'text-left',sWidth:"20%",bSortable : true,
            },
            {   mData:"status",
                    bSortable : false,
                    sClass : 'text-center',
                    sWidth:"10%",
                    mRender: function (v, t, o) {
                        var active_string;
                        if (v == '1') {
                            active_string = "<span class='badge bg-info'>Yes</span>";
                        }else{
                            active_string = "<span class='badge bg-default'>No</span>";
                        }
                        return active_string;
                    },
                },
            {   
                mData:"updated_at",bSortable : false,bVisible:false,
            },
            {
                mData: 'null',
                bSortable: false,
                sWidth: "30px",
                sClass: "text-center",
                mRender: function(v, t, o) {
                    var id= o['id'];
                    var edit_path = "<?=URL::route('users.edit', ['id' => ':id'])?>";
                    edit_path = edit_path.replace(':id',o['id']);
                    var act_html = "<div class='btn-group'>"
                        +"<a href='"+edit_path+"' data-toggle='tooltip' title='Edit' data-placement='top' class='btn btn-xs btn-info p-5' style='font-size:10px; line-height:15px; padding: 6px'><i class='fa fa-fw fa-edit'></i></a>"
                        + "<a href='javascript:void(0);' onclick=\"deleteRecordData('"+delete_path+"','"+title+"','"+text+"','"+token+"','"+type+"','"+table+"',"+id+")\" data-toggle='tooltip' title='Delete' data-placement='top' class='btn btn-xs btn-danger p-5' style='font-size:10px; line-height:15px; padding: 6px;  margin-left: 9px;'><i class='fa fa-fw fa-trash'></i></a>"
                        +"</div>";
                    return act_html;
                },
            }, 
        ],
        fnPreDrawCallback : function() { $("div.overlay").css('display','flex'); },
        fnDrawCallback : function (oSettings) {
            $("div.overlay").hide();
        },
    });
    // $.fn.dataTable.ext.errMode = 'none';
    // $.fn.dataTable.ext.errMode = 'throw';
});



// function userData()
// {
//     $('#add_data').modal('show');
// }
//  $('.select2').select2({
//     placeholder : "status",
// });
 // function ChangeStatus(id,status){
 //        if(status == 0)
 //        {
 //            var title = "Are you sure to active this User?";
 //        }
 //        else
 //        {   
 //            var title = "Are you sure to inactive this User?";
 //        }
 //           swal({
 //            title: title,
 //            text: "",
 //            type: "warning",
 //            showCancelButton: true,
 //            confirmButtonText: "Yes, Change Status!",
 //            cancelButtonText: "No, cancel it!",
 //            closeOnConfirm: true,
 //            closeOnCancel: true
 //            }, function(isConfirm) {
 //                if (isConfirm) {
 //                // updateStatus(act_path,id,token,status);
 //                }
 //            });
 //    }

 //    $(function(){

 //            $('#date_range_1').daterangepicker({
 //                ranges: {
 //                'Today': [moment(), moment()],
 //                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
 //                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
 //                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
 //                'This Month': [moment().startOf('month'), moment().endOf('month')],
 //                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
 //                'This Year': [moment().startOf('year').startOf('days'),moment()],
 //                'Last Year': [moment().subtract('month',12).startOf('days'), moment().subtract(moment(),12).endOf('days')]
 //              },
 //              opens : "right",
 //              startDate: moment(),
 //              endDate: moment()
 //            });

 //            // graph_generator($('#date_range_1').val());

 //            $('#date_range_1').on('apply.daterangepicker', function(ev, picker) {
 //                graph_generator($('#date_range_1').val());
 //            });
 //        });

 function deleteRecordData(delete_path,title,text,token,type,table,id)
{
    swal({
        title: title,
        text: text,
        type: type,
        showCancelButton: true,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function(isConfirm) {
        if (isConfirm) {
            deleteRequest(delete_path,id,token);
        } 
    });
}
</script>
@include('admin.layout.alert')
@stop