@extends('admin.layout.layout')
@section('style')
    <?= Html::style('backend/plugins/jquery-multi-select/css/multi-select.css',[],IS_SECURE) ?>
@stop
@section('start_form')
    <?=Form::model($user, ['method'=>'PATCH','id'=>'user_edit_form', 'class' => 'm-0 form-horizontal'])?>
@stop
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
    <div class="title">
         <h4><?=Breadcrumbs::render('edit_user',$user) ?></h4>
    </div>
    <div class="pl-10">
        <button type="submit" name="save_button" value="save" id="save_exit_1" class="btn btn-primary btn-sm save save_exit" title="Save & Exit" ></i>Save</button>
        <a href="javascript;;" onclick="window.history.go(-1); return false;" class="btn btn-default btn-sm" title="Back to users page">Cancel</a>
    </div>
</nav>
@stop
@section('content')
@include('admin.users._form')
<div id="permission_add" class="row">
    <div class="row">
        @foreach($user_permission as $keys => $single_user_perms)
            <div class="col-md-4 p-10">
                <div class="card">
                    <h5 class="subtitle">
                        <?=  ucwords(str_replace('_',' > ', $keys)) ?>
                    </h5>
                    <hr>
                    <div class="animated-checkbox">
                        <label>
                            <input type="checkbox" class ="sub_selection" value="<?= $keys ?>"><span class="label-text">Select All</span>
                        </label>
                    </div>
                    <?= Form::select('permission[]',$single_user_perms,old('permission',$role_current_permissions), array('multiple'=>true,'class' => 'multi-select user_permission_multi '.$keys)) ?>
                </div>
            </div>
        @endforeach
    </div>
</div>

<div class="text-right">
   <button type="submit" name="save_button" value="save" id="save_exit_2" class="btn btn-primary btn-sm save save_exit" title="Save & Exit" ></i>Save</button>
    <a href="javascript;;" onclick="window.history.go(-1); return false;" class="btn btn-default btn-sm" title="Back to users page">Cancel</a>
</div>
@stop

@section('end_form')
<?=Form::close()?>
@stop
@section('script')
<?= Html::script('backend/js/select2.min.js',[],IS_SECURE) ?> 
<?= Html::script('backend/js/jquery.form.min.js',[],IS_SECURE) ?>
<?= Html::script('backend/plugins/jquery-multi-select/js/jquery.multi-select.js',[],IS_SECURE) ?>

    <script type="text/javascript">
        $('.select2').select2();

        $('.user_permission_multi').multiSelect();

        $('form').submit(function(){
            $('.overlay').show();
        });

        $("#selectall").click(function(){
            var is_checked = $(this).is(':checked');
            $(".sub_selection").prop('checked',is_checked);
            if (is_checked == true) {
                $('.user_permission_multi').multiSelect('select_all');
            }else{
                $('.user_permission_multi').multiSelect('deselect_all');
            }
        });

        $('.sub_selection').click(function(){
            var is_checked = $(this).is(':checked');
            var sub_selection_value = $(this).val();

            if (is_checked == true) {
                $('.'+sub_selection_value).multiSelect('select_all');
            }else{
                $('.'+sub_selection_value).multiSelect('deselect_all');
            }
        })

        $("input[type='checkbox'][class*='select_']").click(function(){
            var getClass = $(this).attr('class');
            var getSection = getClass.split(' ');
            var getSection = getSection[1].slice(7);
            var is_checked = $(this).is(':checked');
            $("."+getSection).prop('checked',is_checked);
        });


         //form submit 
        $(".save").click(function(e){
            var val = $(this).val();
            var method_type = 'PATCH';
            var id = '<?= $id?>';

            var url = "<?= URL::route('users.update',['id'=>':id']) ?>";
            url = url.replace(':id',id);

            var token = "<?=csrf_token()?>";
        
            $('#user_edit_form').ajaxSubmit({
                url: url,
                type: method_type,
                data: { "_token" : token,"id":id },
                dataType: 'json',
                
                beforeSubmit : function()
                {
                    if(val == 'save')
                    {
                        $('.save').attr('disabled',true);
                        $('.save').html('<i class="fa fa-spinner fa-spin"></i> Please wait...');
                    }
                    $("[id$='_error']").empty();
                },
                
                success : function(resp)
                {              
                    $(".overlay").hide();
                    
                    if (resp.success == true) {
                        var  action = resp.action;
                        toastr.success('User successfully '+action);

                        if(val == 'save')
                        {
                            $('.save').attr('disabled',true);
                            $('.save').html('<i class="fa fa-spinner fa-spin"></i> Please wait...');
                            window.location.href = "<?=route('users.index')?>";
                        }
                        else
                        {
                            $('.save').attr('disabled',true);
                            $('.save').html('<i class="fa fa-spinner fa-spin"></i> Please wait...');
                            window.location.href = "<?=route('users.index')?>";
                        }
                    }
                   
                },
                error : function(respObj){    
                    $.each(respObj.responseJSON.errors, function(k,v){
                        $('#'+k+'_error').text(v);
                    });
                    toastr.error('there were some errors!');
                        $('#save_exit_1').attr('disabled',false);
                        $('#save_exit_1').html('Save');
                        $('#save_exit_2').attr('disabled',false);
                        $('#save_exit_2').html('Save');
                    
                    $(".overlay").hide();
                }
            });
        });

        // role change
        $('#role').change(function(){
            var val = $(this).val();
            var url = "<?= URL::route('users.role.get.name')?>";
            var method_type = 'GET';
            var token = "<?=csrf_token()?>";
            // console.log(val);
            $.ajax({
                type:method_type,
                url:url,
                data:{'id': val,'token': token},
                dataType:'json',
                success: function(respObj) {
                    if(respObj.success == true)
                    {
                        $('#permission_add').html(respObj.html);
                    }
                }
            });
        });
    </script>
    @include('admin.layout.alert')
@endsection