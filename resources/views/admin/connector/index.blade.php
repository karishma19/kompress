@extends('admin.layout.layout')
@section('style')
    <?=Html::style('backend/plugins/sweetalert/sweetalert.min.css', [], IS_SECURE)?>
    <?= Html::style('backend/css/dataranger.css',[],IS_SECURE) ?>
    <style type="text/css">
        .box__dragndrop,
        .box__uploading,
        .box__success,
        .box__error {
            display: none;
        }
    </style>
@stop
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
    <div class="title">
        <h4><i class="fa fa-user"></i>Connector</h4>
    </div>
</nav>
@stop
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-title-w-btn">
                <h4 class="title">Connector</h4>
            </div><hr>
            <div class="card-body table-responsive">
                <div class="p-20">
                    <table class="table" id="beam">
                        <thead>
                            <tr>
                                <th>Leap</th>
                                <th>Thickness</th>
                                <th>Nett Weight</th>
                                <th>Nett Weight Unit</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            @include('admin.layout.overlay')
        </div>
    </div>
</div>

@stop
<!-- for error list show in model -->
@section('script')

<?=Html::script('backend/plugins/datatable/jquery.dataTables.min.js', [], IS_SECURE)?>
<?=Html::script('backend/plugins/datatable/dataTables.bootstrap.min.js', [], IS_SECURE)?>
<?=Html::script('backend/plugins/sweetalert/sweetalert.min.js', [], IS_SECURE)?>
<?=Html::script('backend/js/fnstandrow.js', [], IS_SECURE)?>
<?=Html::script('backend/js/delete_script.js', [], IS_SECURE)?>
<?= Html::script('backend/js/select2.min.js',[],IS_SECURE) ?>
<?= Html::script('backend/js/moment.min.js',[],IS_SECURE) ?>
<?= Html::script('backend/js/dateranger.js',[],IS_SECURE) ?>

<script type="text/javascript">
var token = "<?=csrf_token()?>";
  
$(function(){

    $('#beam').DataTable({
        "bProcessing": false,
        "bServerSide": true,
        "autoWidth": false,
        "sAjaxSource": '<?=route('connector.index')?>',
        "aaSorting": [ 1,"desc"],
        "aoColumns": [
            {
                mData:"leap",
                bVisible:true,
            },
            {
                mData:"thickness",
                bVisible:true,
            },
            {
                mData:"nett_weight",
                bVisible:true,
            },
            {
                mData:"nett_weight_unit",
                bVisible:true,
            },
        ],
        fnPreDrawCallback : function() { $("div.overlay").css('display','flex'); },
        fnDrawCallback : function (oSettings) {
            $("div.overlay").hide();
        },
    });
    
});

</script>
@include('admin.layout.alert')
@stop