<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Admin</title>
        <?= Html::style('backend/css/main.css') ?>
	</head>
	<body>
		<section class="material-half-bg" style="background:url('<?=LOGIN_BG?>') no-repeat !important"></section>
		<section class="login-content">
	        <div class="login-box">
	            <div class="login_logo">
	                <p class="m-0"><b>Kompress Admin</b></p>
	            </div>
	            <?= Form::open(['route'=>'admin.login.post','id'=>'login_form','class'=>'login-form', 'method' => 'POST']) ?>
	            		<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="login-head">
				            <h3 class="m-0 text-center">Log in to Your Account</h3>
						</div>
				            <center style="margin-bottom: 10px;">
				            	<span style="color:yellow;"></span>
							</center>				       
						<div class="form-group @if($errors->first('email')) has-error @endif">
							<label class="control-label">USERNAME</label>
							<?= Form::email('email',null,['class'=>'form-control mb-0','autofocus','placeholder'=>'Enter your email']) ?>
							<span style="color:yellow;"><?= $errors->first('email') ?></span>
						</div>
						<div class="form-group @if($errors->first('password')) has-error @endif">
							<label class="control-label">PASSWORD</label>
							<?= Form::password('password',['class'=>'form-control mb-0','placeholder'=>'Enter your password']) ?>
							<span style="color:yellow;"><?= $errors->first('password') ?></span>
						</div>
	                <div class="form-group">
	                    <div class="utility">
	                        <div class="animated-checkbox">
	                            <label class="semibold-text">
	                              <!-- <?=Form::checkbox('name', 'remember');?><span class="label-text">Stay Signed in</span> -->
	                            </label>
	                        </div>
	                        <!-- <p class="semibold-text mb-0"><a id="toFlip" href="#" class="forget_link">Forgot Password ?</a></p> -->
	                    </div>
	                </div>
	                <div class="form-group btn-container">
	                  <button class="btn btn-primary btn-block form-control">SIGN IN <i class="fa fa-sign-in fa-lg"></i></button>
	                </div>
	            <?=Form::close()?>
	            <form id="forget-form" class="forget-form" action="/admin/login">
	                <h3 class="login-head"><i class="fa fa-lg fa-fw fa-lock"></i>Forgot Password ?</h3>
	                <div class="form-group @if($errors->first('email')) has-error @endif">
	                    <label class="control-label"></label>
	                    <?=Form::label('email', 'EMAIL', ['class' => 'control-label']);?>
	                    <?=Form::email('email', null, ['class' => 'form-control', 'autofocus', 'placeholder' => 'Enter your email'])?>
	                    <span id="email_error" class="text-danger help-block"></span>
	                </div>
	                <div class="form-group btn-container">
	                    <button class="btn btn-primary btn-block" type="submit" id="send_mail">SEND <i class="fa fa-unlock fa-lg"></i></button>
	                </div>
	                <div class="form-group mt-20">
	                    <p class="semibold-text mb-0"><a id="noFlip" href="#"><i class="fa fa-angle-left fa-fw"></i> Back to Login</a></p>
	                </div>
	            </form>
	        </div>
    	</section>
	</body>
<!-- js placed at the end of the document so the pages load faster -->
<?= Html::script('backend/js/jquery-2.1.4.min.js')?>
<?= Html::script('backend/js/bootstrap.min.js',[],IS_SECURE) ?>
<?= Html::script('backend/js/essential-plugins.js')?>
<?=Html::script('backend/js/jquery.form.min.js')?>
<?= Html::script('backend/js/jquery.slimscroll.min.js',[],IS_SECURE) ?>
<?= Html::script('backend/plugins/toastr-master/toastr.min.js',[],IS_SECURE) ?>
<?= Html::script('backend/js/main.js',[],IS_SECURE) ?>

<script type="text/javascript">

    var token = "<?=csrf_token()?>";
    $('form').submit(function(){
        $('.overlay').show();
    });
   
</script>
</html>
