@extends('admin.layout.layout')
@section('style')
    <?=Html::style('backend/plugins/sweetalert/sweetalert.min.css', [], IS_SECURE)?>
    <?= Html::style('backend/css/dataranger.css',[],IS_SECURE) ?>
    <style type="text/css">
        .box__dragndrop,
        .box__uploading,
        .box__success,
        .box__error {
            display: none;
        }
    </style>
@stop
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
    <div class="title">
        <h4><i class="fa fa-user"></i>Bracing</h4>
    </div>
</nav>
@stop
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-title-w-btn">
                <h4 class="title">Bracing</h4>
            </div><hr>
            <div class="card-body table-responsive">
                <div class="p-20">
                    <table class="table" id="beam">
                        <thead>
                            <tr>
                                <th>Height</th>
                                <th>Width</th>
                                <th>Minimum Thickness</th>
                                <th>Maximum Thickness</th>
                                <!-- <th>Fold</th> -->
                                <th>Material Type</th>
                                <th>Nett Weight</th>
                                <th>Nett Weight Unit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($bracing as $bracing_value)
                                <tr>
                                    <th>{{$bracing_value['depth']}}</th>
                                    <th>{{$bracing_value['height']}}</th>
                                    <th>{{$bracing_value['min_thickness']}}</th>
                                    <th>{{$bracing_value['max_thickness']}}</th>
                                    <!-- <th>{{$bracing_value['fold']}}</th> -->
                                    <th>{{$bracing_value['material_type']}}</th>
                                    <th>{{$bracing_value['nett_weight']}}</th>
                                    <th>{{$bracing_value['nett_weight_unit']}}</th>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @include('admin.layout.overlay')
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-title-w-btn">
                <h4 class="title">Bracing Length</h4>
            </div><hr>
            <div class="card-body table-responsive">
                <div class="p-20">
                    <table class="table" id="beam">
                        <thead>
                            <tr>
                                <th>Height</th>
                                <th>Horizonal Length</th>
                                <th>Diagonal Length</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($bracing_length as $bracing_length_value)
                                <tr>
                                    <th>{{$bracing_length_value['depth']}}</th>
                                    <th>{{$bracing_length_value['horizonal_length']}}</th>
                                    <th>{{$bracing_length_value['diagonal_length']}}</th>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @include('admin.layout.overlay')
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-title-w-btn">
                <h4 class="title">Diagonal Qty</h4>
            </div><hr>
            <div class="card-body table-responsive">
                <div class="p-20">
                    <table class="table" id="beam">
                        <thead>
                            <tr>
                                <th>Height of system</th>
                                <th>Qty</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($diagonal_qty as $diagonal_qty_value)
                                <tr>
                                    <th>{{$diagonal_qty_value['length']}}</th>
                                    <th>{{$diagonal_qty_value['qty']}}</th>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @include('admin.layout.overlay')
        </div>
    </div>
</div>

@stop
<!-- for error list show in model -->
@section('script')

<?=Html::script('backend/plugins/datatable/jquery.dataTables.min.js', [], IS_SECURE)?>
<?=Html::script('backend/plugins/datatable/dataTables.bootstrap.min.js', [], IS_SECURE)?>
<?=Html::script('backend/plugins/sweetalert/sweetalert.min.js', [], IS_SECURE)?>
<?=Html::script('backend/js/fnstandrow.js', [], IS_SECURE)?>
<?=Html::script('backend/js/delete_script.js', [], IS_SECURE)?>
<?= Html::script('backend/js/select2.min.js',[],IS_SECURE) ?>
<?= Html::script('backend/js/moment.min.js',[],IS_SECURE) ?>
<?= Html::script('backend/js/dateranger.js',[],IS_SECURE) ?>

<script type="text/javascript">

</script>
@include('admin.layout.alert')
@stop