<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		body{
			font-family: Arial, Helvetica, sans-serif;
			color: #333333
		}
		table,td,th{
			border: 1px solid #333;
			border-collapse: collapse;
			vertical-align: top
		}
		.border-bottom td{
			border-bottom: medium none;
			border-top: medium none;
		}
		.border-none td{
			border: medium none;
		}
		body{
			font-size: 14px
		}
		.vertical-align{
			vertical-align: middle;
		}
		td,th{
			padding: 3px 5px
		}
	</style>
</head>
<body>
	<div style="display: inline-block;width: 100%;margin-bottom: 30px;">
		<table style="width: 100%;border:none;">
			<tr>
				<td style="border: none;width: 40%;float: left;padding: 0">
					<table style="border: none;">
						<tr>
							<td style="border: none;width: 30%;padding-left:0;float: left; "><strong>Ref. No.</strong></td>
							<td style="width: 70%;border:none"><strong>K/QE/00/2018.</strong></td>
						</tr>
						<tr>
							<td style="border:none;width: 30%;padding-left:0;float: left; "><strong>Date:</strong></td>
							<td style="width: 70%;border:none"><strong><?= date('d-m-Y') ?></strong></td>
						</tr>
						<tr>
							<td style="border:none;width: 30%;padding-left:0;float: left; "><strong>User:</strong></td>
							<td style="width: 70%;border:none"></td>
						</tr>
						<tr>
							<td style="border: medium none;width: 30%;padding-left:0;float: left; "><strong>Email:</strong></td>
							<td style="width: 70%;border:none"></td>
						</tr>
						<br/>
						<br/>
						<tr>
							<td style="border:none;width: 30%;padding-left:0;float: left; ">To,</td>
							<td style="width: 70%;border:none"></td>
						</tr>
						<tr>
							<td style="border:none;width: 30%;padding-left:0;float: left; ">XYZ Ltd.</td>
							<td style="width: 70%;border:none"></td>
						</tr>
					</table>
				</td>
				<td style="width: 20%;border:none;"></td>
			</tr>
		</table>
	</div>
	<div style="display: inline-block;width: 100%;margin-bottom: 30px">
		<h4 style="text-align: center;"><u>QUOTATION FOR HEAVY DUTY RACKING SYSTEM</u></h4>
		<table style="border: medium none;width: 100%">
			<tr>
				<td style="width: 80%;border: medium none;">LAYOUT NO.:</td>
				<td style="width: 20%;border: medium none;">DATED: <?= date('d-m-Y') ?></td>
			</tr>
		</table>
		<table style="width: 100%;">
			<tr>
				<td style="width: 10%;text-align:center;padding: 0">NO.</td>
				<td style="width: 30%;text-align:center;">SIZE</td>
				<td style="width: 10%;text-align:center;">QTY</td>
				<td style="width: 20%;text-align:center;">TYPE</td>
				<td style="width: 20%;text-align:center;">NO OF LOADING LEVEL</td>
				<td style="width: 10%;text-align:center;">BASIC RATE(₹)</td>
			</tr>
			<?php
                $count = 0;
            ?>
            @if(count($details))
                @foreach($details as $key => $single_detail)
                    @if($key != 'main_unit')
                        <?php
                            $sample = head($single_detail['beam']);
                        ?>
                        <tr>
                            <td style="width: 10%;text-align:center;padding: 0"><?= $count+1 ?></td>
                            <td style="width: 30%;text-align:center;"><?= $sample['beam_type']['load_bearing_capacity'] ?> c/c <?= $sample['beam_type']['min_length'] ?> * <?= $sample['beam_type']['max_length'] ?> mm</td>
                            <td style="width: 30%;text-align:center;">1</td>
                            <td style="width: 30%;text-align:center;">Main Unit</td>
                            <td style="width: 30%;text-align:center;"><?= count($single_detail['load_lavel']) ?></td>
                            <td style="width: 30%;text-align:center;"></td>
                        </tr>
                        <?php 
                            $count++;
                            unset($single_detail['beam']['count']);
                        ?>
                        @foreach($single_detail['beam'] as $single_beam_details)
                            <tr>
                                <td style="width: 10%;text-align:center;padding: 0"><?= $count+1 ?></td>
                                <td style="width: 30%;text-align:center;"><?= $single_beam_details['beam_type']['load_bearing_capacity'] ?> c/c <?= $single_beam_details['beam_type']['min_length'] ?> * <?= $single_beam_details['beam_type']['max_length'] ?> mm</td>
                                <td style="width: 30%;text-align:center;"><?= $single_detail['add_on_unit'] ?></td>
                                <td style="width: 30%;text-align:center;">Add On Units</td>
                                <td style="width: 30%;text-align:center;"><?= count($single_detail['load_lavel']) ?></td>
                                <td style="width: 30%;text-align:center;"></td>
                            </tr>
                            <?php
                                $count++;
                            ?>
                        @endforeach
                    @endif
                @endforeach
            @else
                <tr>
                    <td colspan="6"></td>
                </tr>
            @endif
			<tr>
				<td style="width: 90%;padding: 0;text-align: right;" colspan="5"><strong>TOTAL BASIC PRICE INCLUDING TRANSPORT & INSTALLATION</strong></td>
				<td style="width: 10%;"></td>
			</tr>
		</table>
	</div>
	<div style="width: 100%;">
		<table style="width: 100%;border: medium none;">
			<tr>
				<td style="width: 80%;border: medium none;font-size:25px;">a. Storage Capacity of Pallets</td>
				<td style="width: 10%;border: medium none;font-size:25px;"><?= $pallet ?> Pallets</td>
			</tr>
			<tr>
				<td style="width: 10%;border: medium none;font-size:25px;">b. Load Per Pallet</td>
				<td style="width: 80%;border: medium none;font-size:25px;"><?= $storage ?> Kg UDL</td>
			</tr>
			<tr>
				<td style="width: 100%;border: medium none;font-size:25px;" colspan="2">c. Each Component of the System will be treated with rust preventive treatment and will be powder coated for better finish.</td>
			</tr>
			<tr>
				<td style="width: 80%;border: medium none;font-size:25px;">
				<ul>
					<li>Basic Price of the System (incl. Transport & Installation)</li>
				</ul>
				  </td>
				<td style="width: 20%;border: medium none;font-size:25px;">₹</td>
			</tr>
		</table>
	</div>
	<br style="clear: both">
	<div style="display: inline-block;width: 100%;margin-bottom: 30px">
		<table style="border: medium none;">
			<tr>
				<td style="width: 100%;border: medium none;"><strong>PRICE DETAILS FOR THE ABOVE OFFERED SYSTEM</strong></td>
			</tr>
		</table>
		<table style="width: 100%;">
			<tr>
				<td style="width: 10%;text-align:left;padding: 0;vertical-align:bottom" valign="bottom">*</td>
				<td style="width: 60%;text-align:left;"><strong>Basic Price of the system including <br/>transport & installation charges</strong></td>
				<td style="width: 5%;text-align:center;"></td>
				<td style="width: 5%;text-align:center;"></td>
				<td style="width: 20%;text-align:center;"></td>
			</tr>
			<tr>
				<td style="width: 10%;text-align:center;padding: 0"><strong>ADD:</strong></td>
				<td style="width: 60%;text-align:right;">IGST @</td>
				<td style="width: 5%;text-align:center;">18</td>
				<td style="width: 5%;text-align:center;">%</td>
				<td style="width: 20%;text-align:center;"><strong>-</strong></td>
			</tr>
			<tr>
				<td style="width: 10%;padding: 0;text-align: right;"></td>
				<td style="width: 60%;padding: 0;text-align: right;"><strong>GRAND TOTAL</strong></td>
				<td style="width: 5%;text-align:center;"></td>
				<td style="width: 5%;text-align:center;"></td>
				<td style="width: 20%;text-align:center;"><strong>-</strong></td>
			</tr>
		</table>
	</div>
	<br style="clear: both">
	<div style="display: inline-block;width: 100%;margin-bottom: 30px">
		<table style="border: medium none;">
			<tr>
				<td style="width: 100%;border: medium none;">∗ Terms & Conditions are as per enclosed proforma.</td>
			</tr>
			<br/>
			<br/>
			<tr>
				<td style="width: 100%;border: medium none;">For <strong>KOMPRESS (INDIA) PVT. LTD.</strong></td>
			</tr>
			<br/>
			<br/>
			<br/>
			<tr>
				<td style="width: 100%;border: medium none;"><strong>Shekhar Agrawal</strong></td>
			</tr>
			<tr>
				<td style="width: 100%;border: medium none;"><strong>V.P. Sales & Mktg.</strong></td>
			</tr>
			<tr>
				<td style="width: 100%;border: medium none;"><strong>98210 53763</strong></td>
			</tr>
		</table>
	</div>
	<br style="clear: both">
	<br style="clear: both">
	<br style="clear: both">
	<div style="display: inline-block;width: 100%;margin-bottom: 30px;">
		<h4 style="text-align: center;"><u>TERMS & CONDITIONS</u></h4>
		<table style="width: 100%;border:medium none;">
			<tr>
				<td style="width: 30%;padding-left:0;float: left;border-left: medium none;text-align: center;font-size: 12px;"><strong>PRICE</strong></td>
				<td style="width: 70%; border-right: medium none;font-size: 12px;text-align: justify;">As mentioned in the Quotation.</td>
			</tr>
			<tr>
				<td style="width: 30%;padding-left:0;float: left;border-left: medium none;text-align: center; font-size: 12px;"><strong>INSTALLATION & COMISSIONING CHARGES</strong></td>
				<td style="width: 70%;border-right: medium none;font-size: 12px;text-align: justify;">As mentioned in the Quotation.</td>
			</tr>
			<tr>
				<td style="width: 30%;padding-left:0;float: left;border-left: medium none;text-align: center;font-size: 12px; "><strong>TRANSPORTATION CHARGES</strong></td>
				<td style="width: 70%;border-right: medium none;font-size: 12px;text-align: justify;">As mentioned in the Quotation.</td>
			</tr>
			<tr>
				<td style="width: 30%;padding-left:0;float: left;border-left: medium none;text-align: center;font-size: 12px; "><strong>UNLOADING & SHIFTING OF CONSIGNMENT</strong></td>
				<td style="width: 70%;border-right: medium none;font-size: 12px;text-align: justify;">Unloading of consignment, safe storage at site until installation will be in the consignment at the installation location.</td>
			</tr>
			<tr>
				<td style="width: 30%;padding-left:0;float: left;border-left: medium none;text-align: center;font-size: 12px; "><strong>INCIDENTAL CHARGES</strong></td>
				<td style="width: 70%;border-right: medium none;font-size: 12px;text-align: justify;">Any unforeseen charges such as Mathadi, Kamgar or Union at site shall be to buyer’s account.</td>
			</tr>
			<tr>
				<td style="e;width: 30%;padding-left:0;float: left;border-left: medium none;text-align: center;font-size: 12px; "><strong>ENTRY PERMITS/ WAY BILLS/ ROAD PERMIT</strong></td>
				<td style="width: 70%;border-right: medium none;font-size: 12px;text-align: justify;">All Entry Permits/ Way Bills/ Road Permits/ Declarations/ Documents etc., required to enter your state will have to be provided by buyer. In case you are unable to provide the same, the cost / penalty / expense incurred during transportation shall be to buyer’s account.</td>
			</tr>
			<tr>
				<td style="e;width: 30%;padding-left:0;float: left;border-left: medium none;text-align: center;font-size: 12px; "><strong>DELIVERY TIME</strong></td>
				<td style="width: 70%;border-right: medium none;font-size: 12px;text-align: justify;">10 - 12 Weeks from the date of Confirmed Order along with advance and Approved drawing/layout duly signed, stamped with Color Shade Selection.</td>
			</tr>
			<tr>
				<td style="e;width: 30%;padding-left:0;float: left;border-left: medium none;text-align: center;font-size: 12px; "><strong>STORAGE OF MATERIAL AT SITE</strong></td>
				<td style="width: 70%;border-right: medium none;font-size: 12px;text-align: justify;">The buyer shall store the material securely. During the period of installation, any shortage of material due to pilferage, misplacement etc. at site would be additionally charged to buyer’s account. Further, if due to unavailability of site or any other reason resulting in non-commencement of installation resulting in material damage or pilferage then rectification charges of the same shall be to buyer’s account.</td>
			</tr>
			<tr>
				<td style="e;width: 30%;padding-left:0;float: left;border-left: medium none;text-align: center;font-size: 12px; "><strong>INSTALLATION</strong></td>
				<td style="width: 70%;border-right: medium none;font-size: 12px;text-align: justify;">Installation shall commence only after clearance of commercial commitments and availability of clear site.</td>
			</tr>
			<tr>
				<td style="e;width: 30%;padding-left:0;float: left;border-left: medium none;text-align: center;font-size: 12px; "><strong>PAYMENT TERMS</strong></td>
				<td style="width: 70%;border-right: medium none;font-size: 12px;text-align: justify;">50% as Advance along with Order, 40% against Proforma Invoice and balance 10% on completion of work within 7 days or within 15 days from the date of delivery, whichever is earlier.</td>
			</tr>
			<tr>
				<td style="e;width: 30%;padding-left:0;float: left;border-left: medium none;text-align: center;font-size: 12px; "><strong>ELECTRICAL POINT</strong></td>
				<td style="width: 70%;border-right: medium none;font-size: 12px;text-align: justify;">Where ever required Electrical wiring for providing single / three-phase AC supply through a separate MCB up to point of installation will be done by the buyer. Buyer shall provide a stabilized, uninterrupted power supply with a voltage stabilizer / spike buster.</td>
			</tr>
			<tr>
				<td style="e;width: 30%;padding-left:0;float: left;border-left: medium none;text-align: center;font-size: 12px; "><strong>VALIDITY OF QUOTE</strong></td>
				<td style="width: 70%;border-right: medium none;font-size: 12px;text-align: justify;">This offer is valid for 30 days from the date of issue.</td>
			</tr>
			<tr>
				<td style="e;width: 30%;padding-left:0;float: left;border-left: medium none;text-align: center;font-size: 12px; "><strong>WARRANTY</strong></td>
				<td style="width: 70%;border-right: medium none;font-size: 12px;text-align: justify;">Our products are warranted against workmanship defects for a period of 12 months. Warranty does not cover any damage due to mishandling or misuse.</td>
			</tr>
			<tr>
				<td style="e;width: 30%;padding-left:0;float: left;border-left: medium none;text-align: center;font-size: 12px; "><strong>FORCE MAJEURE</strong></td>
				<td style="width: 70%;border-right: medium none;font-size: 12px;text-align: justify;">In case of performance disruption due to natural disasters (like earthquakes,hurricanes, floods, etc.), wars, riots or other major upheaval, performance failures of parties outside our control (e.g.: telephone service, labor action, etc.) KOMPRESS will not be held liable.</td>
			</tr>
			<tr>
				<td style="e;width: 30%;padding-left:0;float: left;border-left: medium none;text-align: center;font-size:12px;"><strong>COMMERCIAL DETAILS</strong></td>
				<td style="width: 70%;border-right: medium none;font-size: 12px;">
					<table style="border: medium none;">
						<tr>
							<td style="width: 100%;border: medium none;font-size: 12px;"><u>BANK DETAILS:</u></td>
						</tr>
						<tr>
							<td style="width: 40%;border: medium none;font-size: 12px;">TITLE OF A/C</td>
							<td style="width: 60%;border: medium none;font-size: 12px;">: KOMPRESS (INDIA) PVT. LTD.</td>
						</tr>
						<tr>
							<td style="width: 40%;border: medium none;font-size: 12px;">NAME OF BANK</td>
							<td style="width: 60%;border: medium none;font-size: 12px;">: STATE BANK OF INDIA (SION MATUNGA ROAD BRANCH)</td>
						</tr>
						<tr>
							<td style="width: 40%;border: medium none;font-size: 12px;">BANK A/C NO.</td>
							<td style="width: 60%;border: medium none;font-size: 12px;">: 52090707696</td>
						</tr>
						<tr>
							<td style="width: 40%;border: medium none;font-size: 12px;">NEFT- IFSC code No</td>
							<td style="width: 60%;border: medium none;font-size: 12px;">: SBIN0020313</td>
						</tr>
						<tr>
							<td style="width: 100%;border: medium none;font-size: 12px;"><u>TAXATION DETAILS:</u></td>
						</tr>
						<tr>
							<td style="width: 40%;border: medium none;font-size: 12px;">PAN NO</td>
							<td style="width: 60%;border: medium none;font-size: 12px;">: AAACK1741K</td>
						</tr>
						<tr>
							<td style="width: 40%;border: medium none;font-size: 12px;">GST IN NO (MAHARASHTRA)</td>
							<td style="width: 60%;border: medium none;font-size: 12px;">:27AAACK1741K1ZL</td>
						</tr>
						<tr>
							<td style="width: 40%;border: medium none;font-size: 12px;">GST IN NO (RAJASTHAN)</td>
							<td style="width: 60%;border: medium none;font-size: 12px;">: 08AAACK1741K1ZL</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>			
	</div>
	<div style="display: inline-block;width: 100%;margin-bottom: 30px;">
		<table style="border: medium none;">
			<tr>
				<td style="width: 100%;border: medium none;text-align: justify;">Kindly note that, we have given our delivery schedule in good faith and will try to execute the order in accordance with this schedule. However in case of delay in delivery or installation at site due to unavoidable circumstances and reasons beyond our control, we do not agree to payment or liquidated damages or any penalty or any risk purchase by you. In the unlikely case of any litigation, the adjudication would be under the Mumbai Jurisdiction.We do not agree for cancellation or reduction in ordered quantity once the order is placed.</td>
			</tr>
			<br/>
			<tr>	
				<td style="width: 100%;border: medium none;">For <strong>KOMPRESS (INDIA) PVT. LTD.</strong></td>
			</tr>
			<br/>
			<br/>
			<tr>
				<td style="width: 100%;border: medium none;"><strong>Authorized Signatory</strong></td>
			</tr>
		</table>
	</div>
</body>
</html>