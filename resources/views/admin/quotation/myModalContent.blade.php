<div ng-controller="makeQuotationController">
    <script type="text/ng-template" id="myModalContent.html">
        <div class="modal-header">
        	<h3>Quotation</h3>
        </div>
        <div class="modal-body">
        	<div class="table-responsive">
        		<table class="quotation-table">
        			<tr ng-repeat="(key,value) in items">
        				<th width="20%" ng-if='key != "beam" && key != "nutboll"'><% key | removeUnderscores_capitalize%></th>
        				<th width="30%" ng-if='key != "beam" && key != "nutboll"'>Count </th>
        				<td width="50%" ng-if='key != "beam" && key != "nutboll"'><% value.count %></td>
        				<th width="20%" ng-if='key === "beam"'>Beam</th>
        				<td colspan="2" ng-if='key === "beam"' class="border-none">
        					<table>
        						<tr>
        							<th width="30%">Count</th>
        							<td width="50%"><% value.count %></td>
        						</tr>
        						<tr>
        							<th>Weight</th>
        							<td><% value.weight %></td>
        						</tr>
        						<tr>
        							<td colspan="2" class="border-none">
        								<table>
        									<tr>
        										<td class="border-none border-right-block" width="20%">
        											<table>
        												<tr>
        													<th colspan="2">Value</th>
        												</tr>
        												<tr>
        													<th width="50%">Type</th>
        													<th width="50%">Load</th>
        												</tr>
        												<tr ng-repeat="(beam_key,beam_value) in value | limitTo : 2" ng-if="beam_value.value != undefind">
        													<td width="50%"><%beam_value.value.type%></td>
        													<td width="50%"><%beam_value.value.load%></td>
        												</tr>
        											</table>
        										</td>
        										<td class="border-none" width="80%">
        											<table>
        												<tr>
        													<th colspan="7">BeamType</th>
        												</tr>
        												<tr>
        													<th>Depth</th>
        													<th>Height</th>
        													<th>Min thickness</th>
        													<th>Max thickness</th>
        													<th>Min length</th>
        													<th>Max length</th>
        													<th>Load bearing capacity</th>
        												</tr>
        												<tr ng-repeat="(beam_key,beam_value) in value | limitTo : 2" ng-if="beam_value.value != undefind">
        													<td><%beam_value.beam_type.depth%></td>
        													<td><%beam_value.beam_type.height%></td>
        													<td><%beam_value.beam_type.min_thickness%></td>
        													<td><%beam_value.beam_type.max_thickness%></td>
        													<td><%beam_value.beam_type.min_length%></td>
        													<td><%beam_value.beam_type.max_length%></td>
        													<td><%beam_value.beam_type.load_bearing_capacity%></td>
        												</tr>
        											</table>
        										</td>
        									</tr>
        									
        								</table>
        							</td>
        						</tr>
        					</table>
        				</td>
        				<th width="20%" ng-if='key === "nutboll"'>Nutball</th>
        				<td colspan="2" ng-if='key === "nutboll"' class="border-none border-bottom">
        					<table>
        						<tr ng-repeat="(nutboll_key,nutboll_value) in value">
        							<th width="30%">
        								<% nutboll_key | removeUnderscores_capitalize%>
        							</th>
        							<td width="70%">
        								<%nutboll_value%>
        							</td>
        						</tr>
        					</table>
        				</td>
        			</tr>
        		</table>
        	</div>
        </div>
        <div class="modal-footer">
        	<button class="btn btn-primary" ng-click="ok()">OK</button>
        	<button class="btn btn-warning" ng-click="cancel()">Cancel</button>
        </div>
    </script>
</div>