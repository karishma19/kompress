@extends('admin.layout.layout')
@section('top_fixed_content')
	<nav class="navbar navbar-static-top">
	    <div class="title">
	        <h4><i class="fa fa-user"></i>Quotation</h4>
	    </div>
	</nav>
@stop
@section('content')
	<div class="row" ng-app="makeQuotation" ng-controller="makeQuotationController">
		<div class="col-md-12">
			<div class="card">
				<?=Form::open(['id'=>'make_quotation','class'=>'form-horizontal'])?>
					<div class="card-title-w-btn">
	                	<h4 class="title">Create Quotation</h4>
	            	</div><hr>
	            	<div class="card-body">
						<div class="row form-group">
							<div class="col-md-6">
								    <label>Main Unit<sup class="text-danger">*</sup></label>
								    <?=Form::number('main_unit', null, ['class' => 'form-control', 'placeholder' => 'Enter Main Unit','ng-model'=>'quotation_parameter.main_unit','ng-change'=>'makeQuotationLogic()']);?>
								    <span id="main_unit_error" class="help-inline text-danger"><?=$errors->first('main_unit')?></span>
							</div>
							<div class="col-md-6">
								    <label>Add on unit<sup class="text-danger">*</sup></label>
								    <?=Form::number('add_on_unit', null, ['class' => 'form-control', 'placeholder' => 'Enter Add on unit','ng-model'=>'quotation_parameter.add_on_unit','ng-change'=>'makeQuotationLogic()']);?>
								    <span id="add_on_unit_error" class="help-inline text-danger"><?=$errors->first('add_on_unit')?></span>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-6">
				                    <label>Height<sup class="text-danger">*</sup></label>
				                    <?=Form::number('height', null, ['class' => 'form-control', 'placeholder' => 'Enter Height','ng-model'=>'quotation_parameter.height','ng-change'=>'makeQuotationLogic()']);?>
				                    <span id="height_error" class="help-inline text-danger"><?=$errors->first('height')?></span>
							</div>
							<div class="col-md-6">
							        <label>Width<sup class="text-danger">*</sup></label>
							        <?=Form::number('width', null, ['class' => 'form-control', 'placeholder' => 'Enter Width','ng-model'=>'quotation_parameter.width','ng-change'=>'makeQuotationLogic()']);?>
							        <span id="width_error" class="help-inline text-danger"><?=$errors->first('width')?></span>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-6">
						            <label>Depth<sup class="text-danger">*</sup></label>
						            <?=Form::number('depth', null, ['class' => 'form-control', 'placeholder' => 'Enter Depth','ng-model'=>'quotation_parameter.depth','ng-change'=>'makeQuotationLogic()']);?>
						            <span id="depth_error" class="help-inline text-danger"><?=$errors->first('depth')?></span>
							</div>
							<div class="col-md-6">
							        <label>No of levels<sup class="text-danger">*</sup></label>
							        <?=Form::number('lavels', null, ['class' => 'form-control', 'placeholder' => 'Enter No of lavels','ng-model'=>'quotation_parameter.lavels','ng-change'=>'makeQuotationLogic()']);?>
							        <span id="lavels_error" class="help-inline text-danger"><?=$errors->first('lavels')?></span>
							</div>
						</div>
						<div class="row" ng-if="quotation_parameter.lavels > 0">
							<div class="col-md-12">
						        <label>Load per Level<sup class="text-danger">*</sup></label>
								<div ng-repeat="n in range(1,quotation_parameter.lavels)">
									<label>Level <% n %></label>
									<div class="row form-group">
										<div class="col-md-6">
											<select name="load_lavel_type<% n %>" class="form-control" ng-model="quotation_parameter['load_lavel'][n]['type']" placeholder="Select load type" ng-options='option.value as option.name for option in load_type' ng-int="same" ng-change='makeQuotationLogic()'></select>
										</div>
										<div class="col-md-6">
							            	<input type="number" name="load_lavel<% n %>" class="form-control" ng-model="quotation_parameter['load_lavel'][n]['load']" placeholder="Add load" ng-change='makeQuotationLogic()'>
										</div>
									</div>

						        </div>
							</div>
						</div>
	            	</div>
				<?= Form::close() ?>
			</div>
		</div>
		<div class="col-md-12">
			<!-- <div class="card">
				<div class="card-title-w-btn">
                	<h4 class="title">Total Quotation</h4>
            	</div><hr>
            	<div class="card-body">
            		Quotation parameter<br>
            		<div class="row">
            			<div class="col-md-2" ng-repeat="(k,v) in quotation_parameter"><% k %> : <% v %></div>
            		</div><br>
            		Result<br><br>
            		<div class="row">
            			<div class="col-md-2" ng-repeat="(k,v) in total_quotation"><% k %> : <% v %></div>
            		</div>
            	</div>
            </div> -->
            <div class="row">
            	<div class="col-md-12 mb-10">
            		<h4 class="pull-left">Materials</h4>
            		<button class="pull-right btn btn-primary" ng-click="open()">Save</button>
            	</div>
            	<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/up-right-1.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Upright (10 Fold)</h5>
			                <p><b>Count:</b><span ng-init='total_quotation.upright.count = 0'><% total_quotation.upright.count %></span></p>
			                <p><b>Weight:</b><span>0 KG</span></p>
		                </div>
	              </div>
            	</div>
            	<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/up-right-2.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Upright (12 Fold)</h5>
			                <p><b>Count:</b><span>0</span></p>
			                <p><b>Weight:</b><span>0 KG</span></p>
		                </div>
	              </div>
            	</div>
            	<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/beamwithleap.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Beam with leap connector</h5>
			                <p><b>Count:</b><span ng-init="total_quotation.beam.count = 0"><% total_quotation.beam.count %></span></p>
			                <p><b>Weight:</b><span ng-init="total_quotation.beam.weight = 0"><% total_quotation.beam.weight %> </span>KG</p>
		                </div>
	              </div>
            	</div>
            	<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/horizontal.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Horizontal bracing</h5>
			                <p><b>Count:</b><span ng-init="total_quotation.horizontal_bracing.count = 0"><% total_quotation.horizontal_bracing.count %></span></p>
			                <p><b>Weight:</b><span>0 KG</span></p>
		                </div>
	              </div>
            	</div>
            	<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/cross-icon.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Cross bracing</h5>
			                <p><b>Count:</b><span ng-init="total_quotation.cross_bracing.count = 0"><% total_quotation.cross_bracing.count %></span></p>
			                <p><b>Weight:</b><span>0 KG</span></p>
		                </div>
	              </div>
            	</div>
            	<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/connector.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Base plate</h5>
			                <p><b>Count:</b><span ng-init="total_quotation.base_plate.count = 0"><% total_quotation.base_plate.count %></span></p>
			                <p><b>Weight:</b><span>0 KG</span></p>
		                </div>
	              </div>
            	</div>
            	<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/faster-bolt-nut.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Nut & Bolts(Base plate to upright)</h5>
			                <p><b>Count:</b><span ng-init="total_quotation.nutboll.base_to_upright = 0"><% total_quotation.nutboll.base_to_upright %></span></p>
		                </div>
	              </div>
            	</div>
            	<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/faster-bolt-nut.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Nut & Bolts(Bracing to Upright)</h5>
			                <p><b>Count:</b><span ng-init="total_quotation.nutboll.bracing_to_upright = 0"><% total_quotation.nutboll.bracing_to_upright %></span></p>
		                </div>
	              </div>
            	</div>
            	<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/faster-bolt-nut.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Anchor Fastners(For Base plate)</h5>
			                <p><b>Count:</b><span ng-init="total_quotation.nutboll.anchor_fasteners = 0"><% total_quotation.nutboll.bracing_to_upright %></span></p>
		                </div>
	              </div>
            	</div>
            	<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/faster-bolt-nut.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>KLIT for bracing</h5>
			                <p><b>Count:</b><span ng-init="total_quotation.nutboll.klit_bracing = 0"><% total_quotation.nutboll.klit_bracing %></span></p>
		                </div>
	              </div>
            	</div>
            	<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/faster-bolt-nut.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Key for beams</h5>
			                <p><b>Count:</b><span ng-init="total_quotation.nutboll.key_beam = 0"><% total_quotation.nutboll.key_beam %></span></p>
		                </div>
	              </div>
            	</div>
            	<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/faster-bolt-nut.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Rivets for beams GI</h5>
			                <p><b>Count:</b><span ng-init="total_quotation.nutboll.revet_beam = 0"><% total_quotation.nutboll.revet_beam %></span></p>
		                </div>
	              </div>
            	</div>
            	<div class="col-md-12">
            		<h4>Accessories</h4>
            	</div>
            	<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/decking-panel.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Decking panel</h5>
			                <p><b>Count:</b><span>0</span></p>
			                <p><b>Weight:</b><span>0 KG</span></p>
		                </div>
	              </div>
            	</div>
            	<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/load_stop_barriers.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Load stop Barriers</h5>
			                <p><b>Count:</b><span>0</span></p>
			                <p><b>Weight:</b><span>0 KG</span></p>
		                </div>
	              </div>
            	</div>
            	<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/pallet_support.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Pallet support bar</h5>
			                <p><b>Count:</b><span>0</span></p>
			                <p><b>Weight:</b><span>0 KG</span></p>
		                </div>
	              </div>
            	</div>
            	<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/pallet_support_10.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Pallet support bar</h5>
			                <p><b>Count:</b><span>0</span></p>
			                <p><b>Weight:</b><span>0 KG</span></p>
		                </div>
	              </div>
            	</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/row_connector.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Row connector</h5>
			                <p><b>Count:</b><span>0</span></p>
			                <p><b>Weight:</b><span>0 KG</span></p>
		                </div>
	              </div>
            	</div>
            	<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/wall_connector.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Wall connector</h5>
			                <p><b>Count:</b><span>0</span></p>
			                <p><b>Weight:</b><span>0 KG</span></p>
		                </div>
	              </div>
            	</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/column-protector.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Column protector</h5>
			                <p><b>Count:</b><span>0</span></p>
			                <p><b>Weight:</b><span>0 KG</span></p>
		                </div>
	              </div>
            	</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/forklift.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Forklift guide</h5>
			                <p><b>Count:</b><span>0</span></p>
			                <p><b>Weight:</b><span>0 KG</span></p>
		                </div>
	              </div>
            	</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/frame-protector.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Frame protector</h5>
			                <p><b>Count:</b><span>0</span></p>
			                <p><b>Weight:</b><span>0 KG</span></p>
		                </div>
	              </div>
            	</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/tie_member.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Tie member</h5>
			                <p><b>Count:</b><span>0</span></p>
			                <p><b>Weight:</b><span>0 KG</span></p>
		                </div>
	              </div>
            	</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/dropin-anchor.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Dropin anchor fasteners gi platted</h5>
			                <p><b>Count:</b><span>0</span></p>
			                <p><b>Weight:</b><span>0 KG</span></p>
		                </div>
	              </div>
            	</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/pipe-bracing.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Pipe bracing cleat</h5>
			                <p><b>Count:</b><span>0</span></p>
			                <p><b>Weight:</b><span>0 KG</span></p>
		                </div>
	              </div>
            	</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/beam-lock-key.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Beam lock key</h5>
			                <p><b>Count:</b><span>0</span></p>
			                <p><b>Weight:</b><span>0 KG</span></p>
		                </div>
	              </div>
            	</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
		          <div class="widget-small primary coloured-icon">
		          	  	<div class="icon">
		          	  		<img  src="<?='/backend/images/pallet.png'?>" alt="" >
		          	  	</div>
		          	  	<div class="info">
			                <h5>Pallet</h5>
			                <p><b>count:</b><span>0</span></p>
			                <p><b>Weight:</b><span>0 KG</span></p>
		                </div>
	              </div>
            	</div>
			</div>
	</div>
	<div ng-controller="makeQuotationController">
		<script type="text/ng-template" id="myModalContent.html">
			<div class="modal-header">
				<h3>Quotation</h3>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="quotation-table">
						<tr ng-repeat="(key,value) in items">
							<th width="20%" ng-if='key != "beam" && key != "nutboll"'><% key | removeUnderscores_capitalize%></th>
							<th width="30%" ng-if='key != "beam" && key != "nutboll"'>Count </th>
							<td width="50%" ng-if='key != "beam" && key != "nutboll"'><% value.count %></td>
							<th width="20%" ng-if='key === "beam"'>Beam</th>
							<td colspan="2" ng-if='key === "beam"' class="border-none">
								<table>
									<tr>
										<th width="30%">Count</th>
										<td width="50%"><% value.count %></td>
									</tr>
									<tr>
										<th>Weight</th>
										<td><% value.weight %></td>
									</tr>
									<tr>
										<td colspan="2" class="border-none">
											<table>
												<tr>
													<td class="border-none border-right-block" width="20%">
														<table>
															<tr>
																<th colspan="2">Value</th>
															</tr>
															<tr>
																<th width="50%">Type</th>
																<th width="50%">Load</th>
															</tr>
															<tr ng-repeat="(beam_key,beam_value) in value | limitTo : 2" ng-if="beam_value.value != undefind">
																<td width="50%"><%beam_value.value.type%></td>
																<td width="50%"><%beam_value.value.load%></td>
															</tr>
														</table>
													</td>
													<td class="border-none" width="80%">
														<table>
															<tr>
																<th colspan="7">BeamType</th>
															</tr>
															<tr>
																<th>Depth</th>
																<th>Height</th>
																<th>Min thickness</th>
																<th>Max thickness</th>
																<th>Min length</th>
																<th>Max length</th>
																<th>Load bearing capacity</th>
															</tr>
															<tr ng-repeat="(beam_key,beam_value) in value | limitTo : 2" ng-if="beam_value.value != undefind">
																<td><%beam_value.beam_type.depth%></td>
																<td><%beam_value.beam_type.height%></td>
																<td><%beam_value.beam_type.min_thickness%></td>
																<td><%beam_value.beam_type.max_thickness%></td>
																<td><%beam_value.beam_type.min_length%></td>
																<td><%beam_value.beam_type.max_length%></td>
																<td><%beam_value.beam_type.load_bearing_capacity%></td>
															</tr>
														</table>
													</td>
												</tr>
												
											</table>
										</td>
									</tr>
								</table>
							</td>
							<th width="20%" ng-if='key === "nutboll"'>Nutball</th>
							<td colspan="2" ng-if='key === "nutboll"' class="border-none border-bottom">
								<table>
									<tr ng-repeat="(nutboll_key,nutboll_value) in value">
										<th width="30%">
											<% nutboll_key | removeUnderscores_capitalize%>
										</th>
										<td width="70%">
											<%nutboll_value%>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" ng-click="ok()">OK</button>
				<button class="btn btn-warning" ng-click="cancel()">Cancel</button>
			</div>
		</script>
	</div>
@stop
@section('script')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.0/angular.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.9.1/underscore-min.js"></script>
	<script src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.10.0.js"></script>
	<script type="text/javascript">
		var total_product = <?= json_encode($total_product) ?>;
	</script>

	<?=Html::script('backend/js/quotation.js', [], IS_SECURE)?>
@stop