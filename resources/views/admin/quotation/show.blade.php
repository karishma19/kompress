@extends('admin.layout.layout')
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
    <div class="title">
        <h4><i class="fa fa-user"></i>Quotation</h4>
    </div>
</nav>
@stop
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-title-w-btn">
            <h4 class="title">Quotation Details</h4>
        </div><hr>
        <div class="card-body table-responsive">
            <div class="p-20">
                <table class="table" id="beam">
                    <thead>
                        <tr>
                            <th class="text-center">NO</th>
                            <th class="text-center">SIZE</th>
                            <th class="text-center">QTY</th>
                            <th class="text-center">TYPE</th>
                            <th class="text-center">NO OF LOADING LEVEL</th>
                            <th class="text-center">BASIC RATE</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $count = 0;
                        ?>
                        @if(count($details))
                            @foreach($details as $key => $single_detail)
                                @if($key != 'main_unit')
                                    <?php
                                        $sample = head($single_detail['beam']);
                                    ?>
                                    <tr>
                                        <td class="text-center"><?= $count+1 ?></td>
                                        <td class="text-center"><?= $sample['beam_type']['load_bearing_capacity'] ?> c/c <?= $sample['beam_type']['min_length'] ?> * <?= $sample['beam_type']['max_length'] ?> mm</td>
                                        <td class="text-center">1</td>
                                        <td class="text-center">Main Unit</td>
                                        <td class="text-center"><?= count($single_detail['load_lavel']) ?></td>
                                        <td class="text-center"></td>
                                    </tr>
                                    <?php 
                                        $count++;
                                        unset($single_detail['beam']['count']);
                                    ?>
                                    @foreach($single_detail['beam'] as $single_beam_details)
                                        <tr>
                                            <td class="text-center"><?= $count+1 ?></td>
                                            <td class="text-center"><?= $single_beam_details['beam_type']['load_bearing_capacity'] ?> c/c <?= $single_beam_details['beam_type']['min_length'] ?> * <?= $single_beam_details['beam_type']['max_length'] ?> mm</td>
                                            <td class="text-center"><?= $single_detail['add_on_unit'] ?></td>
                                            <td class="text-center">Add On Units</td>
                                            <td class="text-center"><?= count($single_detail['load_lavel']) ?></td>
                                            <td class="text-center"></td>
                                        </tr>
                                        <?php
                                            $count++;
                                        ?>
                                    @endforeach
                                @endif
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6"></td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                <?=Form::open(['route'=>'quotation.generate_pdf','class'=>'form-horizontal'])?>
                    <div class="row form-group">
                        <div class="col-md-5">
                            <label>Storage Capacity of Pallets<sup class="text-danger">*</sup></label>
                            <?=Form::text('storage', null, ['class' => 'form-control', 'placeholder' => 'Enter Storage']);?>
                            <span id="storage_error" class="help-inline text-danger"><?=$errors->first('storage')?></span>
                        </div>
                        <div class="col-md-5">
                            <label>Load Per Pallet<sup class="text-danger">*</sup></label>
                            <?=Form::text('pallet', null, ['class' => 'form-control', 'placeholder' => 'Enter pallet']);?>
                            <span id="pallet_error" class="help-inline text-danger"><?=$errors->first('pallet')?></span>
                        </div>
                        <div class="col-md-2">
                            <button class="pull-right btn btn-primary" type="submit">Generate PDF</button>
                        </div>
                    <?= Form::close() ?>
                </div>
            </div>
        </div>
        @include('admin.layout.overlay')
    </div>
</div>
@stop