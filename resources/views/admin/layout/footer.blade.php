<?=Html::script('backend/js/jquery.min.js', [], IS_SECURE)?>
<?=Html::script('backend/js/bootstrap.min.js', [], IS_SECURE)?>
<?=Html::script('backend/js/jquery.slimscroll.min.js', [], IS_SECURE)?>
<?=Html::script('backend/plugins/toastr-master/toastr.min.js', [], IS_SECURE)?>
<?=Html::script('backend/js/pace.min.js', [], IS_SECURE)?>
<?=Html::script('backend/js/main.js', [], IS_SECURE)?>
<?=Html::script('js/parsley.js', [], IS_SECURE)?>
<?= Html::script('backend/js/timeout.js',[],IS_SECURE) ?>

@yield('script')
<script type="text/javascript">
	@if(count($errors))
		toastr.error('Something went wrong please check');
	@endif
    $(document).ready(function(){

        $('#success').delay(3000).fadeOut('slow');
        $('#danger').delay(3000).fadeOut('slow');
        $('#warning').delay(3000).fadeOut('slow');

        @if(Request::is('admin*'))   
            var logout_url = "<?= URL::to('admin/logout') ?>";
            var redirUrl = '{{ URL::route("admin.locked") }}';
        @endif 
        // 1 sec = 1000 , wanrafter 2 hours 
        $.sessionTimeout({
            logoutUrl       : logout_url,
            redirUrl        : redirUrl,
            warnAfter       : 7200000,
            redirAfter      : 1200000,
            keepAlive       : false,
            countdownMessage: 'Otherwise You will be redirected to login page in {timer} seconds.',
            ignoreUserActivity: false
        });
    });
    $.ajaxSetup({
        statusCode: {
            401: function() {
                swal({
                   title: "Session Timeout",
                   type:"error",
                   timer: 2000,
                   showConfirmButton: false 
                });
                location.reload();
            }
        }
    });
</script>