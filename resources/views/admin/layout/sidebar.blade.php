<aside class="main-sidebar hidden-print">
    <section class="sidebar">
        <div class="search-panel">
            <div class="search-box">
              <div class="icon pull-left"><i class="fa fa-search"></i></div>
              <input class="text" type="text" placeholder="Special Search if require">
              <div class="clearfix"></div>
            </div>
        </div>
        <!-- Sidebar Menu-->
         <div class="sidebar-content">
            <ul class="sidebar-menu" id="nav-accordion">
                <ul class="sidebar-menu" id="nav-accordion">
                    <li class=" @if(Request::segment(2) == 'dashboard') active @endif">
                    <a href="/admin/dashboard">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                    </a>
                    </li>
                    @if(Request::is('admin*'))
                    @if(App\Helpers\RolePermissionCheck::isPermitted('roles.index') ||
                        App\Helpers\RolePermissionCheck::isPermitted('users.index'))
                    <li class="sub-menu @if(Request::segment(2) == 'user_masters') active @endif">
                        <a href="javascript:;">
                            <i class="fa fa-user"></i>
                            <span>User Master</span>
                            <i class="fa fa-angle-right"></i>
                        </a>
                        <ul class="treeview-menu @if(Request::segment(2) == 'user_masters' && (Request::segment(3) == 'roles')) menu-open @endif">
                        @if(App\Helpers\RolePermissionCheck::isPermitted('roles.index'))
                            <li>
                                <a href="<?=route('roles.index')?>" class="@if(Request::segment(3) == 'roles') active @endif">
                                    <i class="fa fa-pencil-square-o"></i>
                                    <span>Roles</span>
                                </a>
                            </li>
                        @endif
                        @if(App\Helpers\RolePermissionCheck::isPermitted('users.index'))
                            <li>
                                <a href="<?=route('users.index')?>" class="@if(Request::segment(3) == 'users') active @endif">
                                    <i class="fa fa-user"></i>
                                    <span>Users</span>
                                </a>
                            </li>
                        @endif
                        </ul>
                    </li>
                    @endif
                    @endif
                </ul>
                <li class="spacer"></li>
                <li class="logout"><a href="javascript:;"><i class="fa fa-user"></i>
                <span><?= auth('admin')->user()->name ?></span>
                <i class="fa fa-angle-right"></i></a>
                    <ul class="logout-modal">
                        <li><a href="/admin/change-password">Change Password</a></li>
                        <li><a href="<?= route('admin.logout') ?>"> Logout</a></li>
                    </ul>
                </li>
                
            </ul>
        </div>
    </section>
</aside>