<header class="main-header">
    <nav class="navbar navbar-static-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="{{route('admin.dashboard')}}" class="navbar-brand"><img src="<?='/backend/images/kompressindia-weblogo.png'?>" alt=""></a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                <i class="fa fa-bars"></i>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="@if(Request::segment(2) == 'quotation') active @endif"><a href="{{route('quotation.index')}}">Quotation</a></li>
                    <li class="@if(Request::segment(2) == 'base_plate') active @endif"><a href="{{route('base_plate.index')}}">Base Plate</a></li>
                    <li class="@if(Request::segment(2) == 'beam') active @endif"><a href="{{route('beam.index')}}">Beam</a></li>
                    <li class="@if(Request::segment(2) == 'connector') active @endif"><a href="{{route('connector.index')}}">Connector</a></li>
                    <li class="@if(Request::segment(2) == 'bracing') active @endif"><a href="{{route('bracing.index')}}">Bracing</a></li>
                    <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                        </ul>
                        </li> -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs"><i class="fa fa-user"></i> {{Auth::guard('admin')->user()->name}}</span>
                        </a>
                        <!-- <ul class="dropdown-menu" style="display: block;">
                            <li>
                            <a href="/admin/change-password" class="">Change Password</a>
                            </li>
                            <li>
                            <a href="http://erpproductsystem.local/admin/logout" class="">Logout</a>
                            </li>
                            </ul> -->
                        <ul class="dropdown-menu" style="width: 200px;box-shadow:0 0 10px rgba(0, 0, 0, 0.2);">
                            <li>
                                <a href="{{route('admin.logout')}}" class="">Sign out</a>
                            </li>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-custom-menu -->
        </div>
        <!-- /.container-fluid -->
    </nav>
</header>