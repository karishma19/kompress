@extends('admin.layout.layout')
@section('style')
@stop
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
    <div class="title">
        <h4>Role</h4>
    </div>
    <div class="top_filter"></div>
    <div class="pl-10"> 
          <a href="<?=route('roles.create')?>" class="btn btn-primary btn-sm" title="Add Role">Add Role</a>
    </div>
</nav>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card p-0">
            <div class="card-body table-responsive">
                <div class="text-right">
                    <div class="number-delete">
                        <ul>
                            <li>
                                <p class="mb-0"><span class="num_item"></span>Item Selected.</p>
                            </li>
                            <li class="bulk-dropdown"><a href="javascript:;">Bulk actions<span class="caret"></span></a>
                                <div class="bulk-box">
                                    <div class="bulk-tooltip"></div>
                                    <ul class="bulk-list">
                                        <li><a href="javascript:void(0);" id="delete" class="delete-btn">Delete selected roles</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>                
                <div class="p-20">
                    <table class="table" id="role_table">
                        <thead>
                            <tr>
                                <!-- <th class="select-all no-sort">
                                    <div class="animated-checkbox">
                                        <label class="m-0">
                                            <input type="checkbox" id="checkAll"/>
                                            <span class="label-text"></span>
                                        </label>
                                    </div>
                                </th> -->
                                <th></th>
                                <th></th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            @include('admin.layout.overlay')
        </div>
    </div>
</div>

@stop

@section('script')
<?= Html::script('backend/js/jquery.form.min.js',[],IS_SECURE) ?>
<?=Html::script('backend/plugins/datatable/jquery.dataTables.min.js', [], IS_SECURE)?>
<?=Html::script('backend/plugins/datatable/dataTables.bootstrap.min.js', [], IS_SECURE)?>
<?=Html::script('backend/plugins/sweetalert/sweetalert.min.js', [], IS_SECURE)?>
<?=Html::script('backend/js/fnstandrow.js', [], IS_SECURE)?>
<?=Html::script('backend/js/delete_script.js', [], IS_SECURE)?>
<?= Html::script('backend/js/select2.min.js',[],IS_SECURE) ?>
<?= Html::script('backend/js/moment.min.js',[],IS_SECURE) ?>
<?= Html::script('backend/js/dateranger.js',[],IS_SECURE) ?>
<script type="text/javascript">

var table = "role_table";
var title = "Are you sure to delete this Customer?";
var text = "You will not be able to recover this record";
var type = "warning";
var delete_path = "";
var act_path = "<?= route('roles.status') ?>";
var token = "<?=csrf_token()?>";
var page_url = "<?=route('roles.index')?>";

$(function(){
    $('#delete').click(function(){
        var delete_id = $('#'+table+' tbody .checkbox:checked');
        checkLength(delete_id);
    });

    $('#role_table').DataTable({
        "bProcessing": false,
        "bServerSide": true,
        "autoWidth": false,
        "sAjaxSource": page_url,
        "aaSorting": [ 1,"desc"],
        "aoColumns": [
            {   
                mData: "id",
                bSortable:false,
                sWidth:"2%",
                bVisible:false,
                sClass:"text-left",   
                mRender: function (v, t, o) {
                    return '<div class="animated-checkbox"><label class="m-0"><input class="checkbox" type="checkbox" id="chk_'+v+'" name="special_id['+v+']" value="'+v+'"/><span class="label-text"></span></label></div>';
                },
            },
            {   mData:"updated_at",sWidth:"20%",bVisible :false,
            },
            {   mData:"name",sWidth:"75%",bSortable: true,
                        // mRender: function(v, t, o) {
                        //     var edit_path = "<?= URL::route('roles.edit',array(':id')) ?>";
                        //     edit_path = edit_path.replace(':id',o['id']);

                        //     var act_html  = '<a title="Edit role" href="'+edit_path+'">'+ v +'</a>'
                                            
                        //     return act_html;
                        // }
            },
            {
                    mData: 'status',
                    bSortable: false,
                    sWidth: "10px",
                    sClass:"text-left", 
                    mRender: function(v, t, o) {
                            if (v == 1) {
                                return "<button onclick='ChangeStatus("+o.id+","+o.status+")' class='btn btn-success btn-sm' style='font-size: 11px;padding:6px 13px 6px 13px;'>Active</button> ";
                            }else{
                                return "<button onclick='ChangeStatus("+o.id+","+o.status+")' class='btn btn-danger btn-sm' style='font-size: 11px;padding: 6px;'>Inactive</button>";
                            }
                        return act_html;
                    }
                },
                {
                mData: 'null',
                bSortable: false,
                sWidth: "30px",
                sClass: "text-center",
                mRender: function(v, t, o) {
                    var id= o['id'];
                    var edit_path = "<?=URL::route('roles.edit', ['id' => ':id'])?>";
                    edit_path = edit_path.replace(':id',o['id']);
                    var act_html = "<div class='btn-group'>"
                        +"<a href='"+edit_path+"' data-toggle='tooltip' title='Edit' data-placement='top' class='btn btn-xs btn-info p-5' style='font-size:10px; line-height:15px; padding: 6px'><i class='fa fa-fw fa-edit'></i></a>"+
                        "</div>";
                    return act_html;
                },
            }, 
            
        ],
        fnPreDrawCallback : function() { $("div.overlay").css('display','flex'); },
        fnDrawCallback : function (oSettings) {
            $("div.overlay").hide();
        },
    });
});

var token = "<?= csrf_token() ?>";

function deleteRecordData(delete_path,title,text,token,type,id)
{
    swal({
        title: title,
        text: text,
        type: type,
        showCancelButton: true,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function(isConfirm) {
        if (isConfirm) {
            deleteRequest(delete_path,id,token);
        } 
    });
}
function ChangeStatus(id,status){
    if(status == 0)
        {
            var title = "Are you sure to active this role?";
        }
        else
        {   
            var title = "Are you sure to inactive this role?";
        }
       swal({
        title: title,
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, Change Status!",
        cancelButtonText: "No, cancel it!",
        closeOnConfirm: true,
        closeOnCancel: true
        }, function(isConfirm) {
            if (isConfirm) {
            updateStatus(act_path,id,token,status);
            }
        });
    }

function updateStatus(act_path,id,token,status)
{
    $.ajax({
        type: "GET",
        url: act_path,
        data: {'id':id, 'status':status,'token':token},
        success: function (data) {
                if(status == 1){
                 toastr.success('Role inactivated successfully',{
                                "showDuration":"1000",
                                "timeOut" : "8000",
                                "iconClass" : "toast toast-success",
                                "closeButton": true,
                                "positionClass": "toast-top-right"
                                 });
                }
                else
                {
                    toastr.success('Role activated successfully',{
                                "showDuration":"1000",
                                "timeOut" : "8000",
                                "iconClass" : "toast toast-success",
                                "closeButton": true,
                                "positionClass": "toast-top-right"
                                 });
                }
                setTimeout(function() {
                    location.reload();
                }, 1000);
            }
    });
}
</script>
@include('admin.layout.alert')
@stop