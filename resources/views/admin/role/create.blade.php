@extends('admin.layout.layout')
@section('style')
    <?= Html::style('backend/plugins/jquery-multi-select/css/multi-select.css',[],IS_SECURE) ?>
@stop
<?= Form::open(['id'=>'role_create_form','class'=>'m-0 form-horizontal']) ?>
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
    <div class="title">
        <h4><?=Breadcrumbs::render('create_role') ?></h4>
    </div>
    <div class="top_filter"></div>
    <div class="pl-10">
        <button type="submit" name="save_button" value="save_new" id="save_new_1" class="btn btn-primary btn-sm save save_new" title="Save role & Add New">Save & Add New</button>
        <button type="submit" name="save_button" value="save" id="save_exit_1" class="btn btn-primary btn-sm save save_exit" title="Save & Exit" ></i>Save</button>
        <a href="<?=route('roles.index')?>" class="btn btn-default btn-sm" title="back to role page">CANCEL</a>
    </div>
</nav>
@stop

@section('content')

    <div class="relative">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="panel-heading">
                        <div class="card-body">
                            <div class="form-group">
                                <label class="control-label col-md-2">Name of Role<sup class="text-danger">*</sup></label>
                                <div class="col-md-8">
                                    <?= Form::text('name',old('name'), ['class' => 'form-control', 'placeholder' => 'Enter Role']); ?>
                                    <span id="name_error" class="help-inline text-danger"><?= $errors->first('name') ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-8">
                                <div class="animated-checkbox">
                                    <label>
                                        <input type="checkbox" id="selectall"><span class="label-text">Click Here To Select All Permission</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Description of Role</label>
                        <div class="col-md-8">
                            <?= Form::text('description',old('description'), ['class' => 'form-control', 'placeholder' => 'Enter Description']); ?>
                        </div>
                    </div>
                    <center><span id="permission_error" class="text-danger text-center"><?= $errors->first('permission') ?></span></center>
                    </div>
                </div>
            </div>
        </div>
       
        <div class="row">
        @foreach($user_permission as $keys => $single_user_perms)
            <div class="col-md-4 p-10">
                <div class="card">
                    <h5 class="subtitle">
                        <?=  ucwords(str_replace('_',' > ', $keys)) ?>
                    </h5>
                    <hr>
                    <div class="animated-checkbox">
                        <label>
                            <input type="checkbox" class ="sub_selection" value="<?= $keys ?>"><span class="label-text">Select All</span>
                        </label>
                    </div>
                    <?= Form::select('permission[]',$single_user_perms,null, array('multiple'=>true,'class' => 'multi-select user_permission_multi '.$keys)) ?>
                </div>
            </div>
        @endforeach
    </div>
    </div>
             
<div class="text-right">
    <button type="submit" name="save_button" value="save_new" id="save_new_2" class="btn btn-primary btn-sm save save_new" title="Save role & Add New" >Save & Add New</button>
    <button type="submit" name="save_button" value="save" id="save_exit_2" class="btn btn-primary btn-sm save save_exit" title="Save & Exit" ></i>Save</button>
    <a href="<?=route('roles.index')?>" class="btn btn-default btn-sm" title="back to role page">CANCEL</a>
</div>
<?= Form::close() ?>
@stop

@section('script')
 <?= Html::script('backend/plugins/jquery-multi-select/js/jquery.multi-select.js',[],IS_SECURE) ?>
 <?= Html::script('backend/js/jquery.form.min.js',[],IS_SECURE) ?>

<script type="text/javascript">
        $('.user_permission_multi').multiSelect();
        $('.user_permission_multi').multiSelect('deselect_all');

        $('form').submit(function(){
            $('.overlay').show();
        });

        $("#selectall").click(function(){
            var is_checked = $(this).is(':checked');
            $(".sub_selection").prop('checked',is_checked);
            if (is_checked == true) {
                $('.user_permission_multi').multiSelect('select_all');
            }else{
                $('.user_permission_multi').multiSelect('deselect_all');
            }
        });

        $('.sub_selection').click(function(){
            var is_checked = $(this).is(':checked');
            var sub_selection_value = $(this).val();

            if (is_checked == true) {
                $('.'+sub_selection_value).multiSelect('select_all');
            }else{
                $('.'+sub_selection_value).multiSelect('deselect_all');
            }
        })

        $("input[type='checkbox'][class*='select_']").click(function(){
            var getClass = $(this).attr('class');
            var getSection = getClass.split(' ');
            var getSection = getSection[1].slice(7);
            var is_checked = $(this).is(':checked');
            $("."+getSection).prop('checked',is_checked);
        });


        // role create
        $(".save").click(function(e){
            var val = $(this).val();
            var url = "<?= URL::route('roles.store') ?>";
            var method_type = 'POST';
            var token = "<?=csrf_token()?>";
        
            $('#role_create_form').ajaxSubmit({
                url: url,
                type: method_type,
                data: { "_token" : token },
                dataType: 'json',
                
                beforeSubmit : function()
                {
                    if(val == 'save_new')
                    {
                        $('#save_new_1').attr('disabled',true);
                        $('#save_new_1').html('<i class="fa fa-spinner fa-spin"></i> Please wait...');
                        $('#save_new_2').attr('disabled',true);
                        $('#save_new_2').html('<i class="fa fa-spinner fa-spin"></i> Please wait...');
                    }
                    else
                    {
                        $('#save_exit_1').attr('disabled',true);
                        $('#save_exit_1').html('<i class="fa fa-spinner fa-spin"></i> Please wait...');
                        $('#save_exit_2').attr('disabled',true);
                        $('#save_exit_2').html('<i class="fa fa-spinner fa-spin"></i> Please wait...');
                    }
                    $("[id$='_error']").empty();
                },
                
                success : function(resp)
                {             
                    $(".overlay").hide();
                    
                    if (resp.success == true) {
                        var  action = resp.action;
                        toastr.success('Role successfully '+action);

                        if(val == 'save_new')
                        {
                            $('#save_new_1').attr('disabled',false);
                            $('#save_new_1').html('Save & New');
                            $('#save_new_2').attr('disabled',false);
                            $('#save_new_2').html('Save & New');
                            window.location.href = "<?=route('roles.create')?>";
                        }
                        else
                        {
                            $('#save_exit_1').attr('disabled',false);
                            $('#save_exit_1').html('Save & Exit');
                            $('#save_exit_2').attr('disabled',false);
                            $('#save_exit_2').html('Save & Exit');
                            window.location.href = "<?=route('roles.index')?>";
                        }
                    }
                   
                },
                error : function(respObj){    

                    $.each(respObj.responseJSON.errors, function(k,v){
                        $('#'+k+'_error').text(v);
                    });
                    toastr.error('there were some errors!');
                    if(val == 'save_new')
                    {
                        $('#save_new_1').attr('disabled',false);
                        $('#save_new_1').html('Save & New');
                        $('#save_new_2').attr('disabled',false);
                        $('#save_new_2').html('Save & New');
                    }
                    else
                    {
                        $('#save_exit_1').attr('disabled',false);
                        $('#save_exit_1').html('Save');
                        $('#save_exit_2').attr('disabled',false);
                        $('#save_exit_2').html('Save');
                    }
                    $(".overlay").hide();
                }
            });
        });

    </script>
@stop
