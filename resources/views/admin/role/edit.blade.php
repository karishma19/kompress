@extends('admin.layout.layout')
@section('style')
    <?= Html::style('backend/plugins/jquery-multi-select/css/multi-select.css',[],IS_SECURE) ?>
@stop
<?= Form::model($role_details,['id'=>'role_edit_form','class'=>'m-0 form-horizontal']) ?>
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
    <div class="title">
       <h4><?=Breadcrumbs::render('edit_role',$role_details) ?></h4>
    </div>
    <div class="top_filter"></div>
    <div class="pl-10">
        <button type="submit" name="save_button" value="save" id="save_exit_1" class="btn btn-primary btn-sm save save_exit" title="Save" ></i>Save</button>
        <a href="<?=route('roles.index')?>" class="btn btn-default btn-sm" title="back to role page">CANCEL</a>
    </div>
</nav>
@stop

@section('content')
    <div class="relative">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="panel-heading">
                        <div class="card-body">
                            <div class="form-group">
                                <label class="control-label col-md-2">Name of Role</label>
                                <div class="col-md-8">
                                    <?= Form::text('name',old('name'), ['class' => 'form-control', 'placeholder' => 'Enter Role']); ?>
                                    <span id="name_error" class="help-inline text-danger"><?= $errors->first('name') ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                            <div class="animated-checkbox">
                                <label>
                                    <input type="checkbox" id="selectall"><span class="label-text">Click Here To Select All Permission</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Description of Role</label>
                        <div class="col-md-8">
                            <?= Form::text('description',old('description'), ['class' => 'form-control', 'placeholder' => 'Enter Description']); ?>
                        </div>
                    </div>
                    <h4 class="text-danger text-center"><?= $errors->first('permission') ?></h4>
                    </div>
                </div>
            </div>
        </div>
       
        <div class="row">
        @foreach($user_permission as $keys => $single_user_perms)
            <div class="col-md-4 p-10">
                <div class="card">
                    <h5 class="subtitle">
                        <?=  ucwords(str_replace('_',' > ', $keys)) ?>
                    </h5>
                    <hr>
                    <div class="animated-checkbox">
                        <label>
                            <input type="checkbox" class ="sub_selection" value="<?= $keys ?>"><span class="label-text">Select All</span>
                        </label>
                    </div>
                    <?= Form::select('permission[]',$single_user_perms,old('permission',$role_current_permissions), array('multiple'=>true,'class' => 'multi-select user_permission_multi '.$keys)) ?>
                </div>
            </div>
        @endforeach
    </div>
    </div>
             
<div class="text-right">
    
    <button type="submit" name="save_button" value="save" id="save_exit_2" class="btn btn-primary btn-sm save save_exit" title="Save" ></i>Save</button>
    <a href="<?=route('roles.index')?>" class="btn btn-default btn-sm" title="back to role page">CANCEL</a>
</div>
<?= Form::close() ?>
@stop

@section('script')
    <?= Html::script('backend/plugins/jquery-multi-select/js/jquery.multi-select.js',[],IS_SECURE) ?>

    <?= Html::script('backend/js/jquery.form.min.js',[],IS_SECURE) ?>
<script type="text/javascript">
        $('.user_permission_multi').multiSelect();

        $('form').submit(function(){
            $('.overlay').show();
        });

        $("#selectall").click(function(){
            var is_checked = $(this).is(':checked');
            $(".sub_selection").prop('checked',is_checked);
            if (is_checked == true) {
                $('.user_permission_multi').multiSelect('select_all');
            }else{
                $('.user_permission_multi').multiSelect('deselect_all');
            }
        });

        $('.sub_selection').click(function(){
            var is_checked = $(this).is(':checked');
            var sub_selection_value = $(this).val();

            if (is_checked == true) {
                $('.'+sub_selection_value).multiSelect('select_all');
            }else{
                $('.'+sub_selection_value).multiSelect('deselect_all');
            }
        })

        $("input[type='checkbox'][class*='select_']").click(function(){
            var getClass = $(this).attr('class');
            var getSection = getClass.split(' ');
            var getSection = getSection[1].slice(7);
            var is_checked = $(this).is(':checked');
            $("."+getSection).prop('checked',is_checked);
        });


        //form submit 
        $(".save").click(function(e){
            var val = $(this).val();
            var method_type = 'PATCH';
            var id = '<?= $id?>';

            var url = "<?= URL::route('roles.update',['id'=>':id']) ?>";
            url = url.replace(':id',id);

            var token = "<?=csrf_token()?>";
        
            $('#role_edit_form').ajaxSubmit({
                url: url,
                type: method_type,
                data: { "_token" : token,"id":id },
                dataType: 'json',
                
                beforeSubmit : function()
                {
                    if(val == 'save')
                    {
                        $('.save').attr('disabled',true);
                        $('.save').html('<i class="fa fa-spinner fa-spin"></i> Please wait...');
                    }
                    $("[id$='_error']").empty();
                },
                
                success : function(resp)
                {              
                    $(".overlay").hide();
                    
                    if (resp.success == true) {
                        var  action = resp.action;
                        toastr.success('role successfully '+action);

                        if(val == 'save')
                        {
                            $('.save').attr('disabled',true);
                            $('.save').html('<i class="fa fa-spinner fa-spin"></i> Please wait...');
                            window.location.href = "<?=route('roles.index',['type'=>'all'])?>";
                        }
                        else
                        {
                            $('.save').attr('disabled',true);
                            $('.save').html('<i class="fa fa-spinner fa-spin"></i> Please wait...');
                            window.location.href = "<?=route('roles.index',['type'=>'all'])?>";
                        }
                    }
                   
                },
                error : function(respObj){    

                    $.each(respObj.responseJSON.errors, function(k,v){
                        $('#'+k+'_error').text(v);
                    });
                    if(val == 'save')
                    {
                        $('.save').attr('disabled',true);
                        $('.save').html('<i class="fa fa-spinner fa-spin"></i> Please wait...');
                        
                    }
                    $(".overlay").hide();
                }
            });
        });
    </script>
@stop
