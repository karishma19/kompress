var makeQuotation = angular.module('makeQuotation',['ui.bootstrap'],function($interpolateProvider){ 
	$interpolateProvider.startSymbol('<%'); 
	$interpolateProvider.endSymbol('%>'); 
}); 


var ModalInstanceCtrl = function ($rootScope,$scope, $modalInstance, items) {

	$scope.items = $rootScope.total_quotation;

	$scope.ok = function () {
		$modalInstance.close($scope.selected);
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
};

makeQuotation.controller('makeQuotationController', function($rootScope,$scope,$http,$modal){
	$scope.quotation_parameter = {
		main_unit : 1,
	};

	$scope.open = function () {
		var modalInstance = $modal.open({
			templateUrl: 'myModalContent.html',
			controller: ModalInstanceCtrl,
			resolve: {
				items: function () {
					return $scope.items;
				}
			}
		});

		modalInstance.result.then(function (selectedItem) {
			$scope.selected = selectedItem;
		}, function () {
		});
	};

	$rootScope.total_quotation = {};
	
	var base_plate = total_product['base_plate'];
	var beam = total_product['beam'];
	var bracing = total_product['bracing'];
	var bracing_length = total_product['bracing_length'];
	var connector = total_product['connector'];
	var diagonal_qty = total_product['diagonal_qty'];

	$scope.range = function(min, max, step) {
	    step = step || 1;
	    var input = [];
	    for (var i = min; i <= max; i += step) {
	        input.push(i);
	    }
	    return input;
	};

	$scope.checkedLoad = function(single_unit){
		if ($scope.quotation_parameter[single_unit] != undefined) {
			$scope.quotation_parameter[single_unit]['same_level'] = true;
		}
	}

	$scope.calculateTotalVolume = function(l,w,h,n,we){
		return (((l * w * h) * n) * we).toFixed(2);
	}

	//count parameter
	

	//change parameter on change of input parameter
	
	$scope.makeQuotationLogic = function(){
		// console.log($scope.quotation_parameter);
		$scope.main_unit = $scope.quotation_parameter['main_unit'];
		// $scope.add_on_unit = $scope.quotation_parameter['add_on_unit'];
		// $scope.levels = $scope.quotation_parameter['levels'];
		// $scope.height = $scope.quotation_parameter['height'];
		// $scope.width = $scope.quotation_parameter['width'];

		$scope.countUpright();
		// $scope.basePlate();
		// $scope.horizontalbracing();
		// $scope.countCrossBracing();
		// $scope.countBeam();
		// $scope.nutBoll();
	}

	//count upright
	$scope.countUpright = function(){
		//upright
		temp_upright = {};
		temp_upright_count = 0;

		angular.forEach($scope.quotation_parameter,function(v,k){
			if (v.add_on_unit != undefined) {
				temp_upright_count = temp_upright_count + v.add_on_unit;
			}
		});

		temp_upright['count'] = ($scope.main_unit * 4) + (temp_upright_count * 2);

		$rootScope.total_quotation['upright'] = temp_upright;

		console.log($rootScope.total_quotation)
	}

	//base plate
	// $scope.basePlate = function(){

	// 	temp_baseplate = {};
	// 	temp_baseplate['count'] = $rootScope.total_quotation['upright']['count'];

	// 	$rootScope.total_quotation['base_plate'] = temp_baseplate;
	// }

	// $scope.horizontalbracing = function(){
	// 	//bracing
	// 	temp_horizontal_bracing = {};
	// 	temp_horizontal_bracing['count'] = $rootScope.total_quotation['upright']['count'];

	// 	$rootScope.total_quotation['horizontal_bracing'] = temp_horizontal_bracing;
	// }

	// $scope.checkLength = function(check_array,check_value,set_value){
	// 	var keepGoing = true;

	// 	var qty = 0;

	// 	if (check_value != undefined && keepGoing == true) {
	// 		angular.forEach(check_array,function(v,k){
	// 			var previous_check = check_array[k-1];
				
	// 			if (previous_check == undefined ) {
	// 				previous_check = 0;
	// 			}else{
	// 				previous_check = check_array[k-1]['length'] + 1;
	// 			}

	// 			if (check_value <= v['length'] && check_value >= previous_check) {
	// 				keepGoing = false;
	// 				qty = v.qty;
	// 			}
	// 		})
	// 	}

	// 	return qty;
	// }

	// //count cross bracing
	// $scope.countCrossBracing = function(){

	// 	temp_cross_bracing = {};
	// 	temp_cross_bracing['count'] = ($scope.checkLength(diagonal_qty,$scope.height) * ($rootScope.total_quotation['upright']['count'] / 2))

	// 	$rootScope.total_quotation['cross_bracing'] = temp_cross_bracing;
	// }

	// function add() {
	// 	return $('.add').toArray().reduce(function(sum,element) {
	// 		return sum + Number(element.value);
	// 	}, 0);
	// }

	// //count beam
	// $scope.countBeam = function(){
	// 	temp_beam = {};
		
	// 	var total_single_beam = $scope.main_unit + $scope.add_on_unit;

	// 	temp_beam['count']  = ($scope.main_unit + $scope.add_on_unit) * ($scope.levels * 2);
	// 	temp_beam['weight'] = 0;

	// 	angular.forEach($scope.quotation_parameter['load_lavel'],function(iv,ik){
			
	// 		var check_beam = [];

	// 		angular.forEach(beam,function(v,k){
	// 			if ($scope.width <= k){
	// 				check_beam.push(v);
	// 			};
	// 		});

	// 		check_beam = _.first(check_beam);
	// 		check_beam = _.sortBy(check_beam,'load_bearing_capacity').reverse();

	// 		var main_beam = _.map(check_beam,function(v){
	// 			if (v.load_bearing_capacity >= iv.load) {
	// 				temp_beam[ik] = {};
	// 				temp_beam[ik]['value'] = iv;
	// 				temp_beam[ik]['weight'] = parseFloat($scope.calculateTotalVolume(v.height,v.depth,v.min_thickness,total_single_beam,v.nett_weight));
	// 				temp_beam[ik]['beam_type'] = v;

	// 				return;
	// 			}
	// 		})
	// 	});

	// 	var weight_array = _.pluck(temp_beam,'weight');

	// 	var sum = 0;

	// 	$.each(weight_array,function(k,v){
	// 		if (v !== undefined) {
	// 			sum = sum + v;
	// 		}
	// 	});

	// 	temp_beam['weight'] = sum;
		
	// 	$rootScope.total_quotation['beam'] = temp_beam;
	// };

	// $scope.nutBoll = function(){
	// 	var beam_count = $rootScope.total_quotation['beam']['count'];
	// 	var upright_count = $rootScope.total_quotation['upright']['count'];

	// 	temp_nut_boll = {};

	// 	temp_nut_boll['base_to_upright'] = upright_count * 4;
	// 	temp_nut_boll['bracing_to_upright'] = ($rootScope.total_quotation['cross_bracing']['count'] / 2) +  ($rootScope.total_quotation['horizontal_bracing']['count'] / 2) + 2;
	// 	temp_nut_boll['anchor_fasteners'] = $rootScope.total_quotation['base_plate']['count'];
	// 	temp_nut_boll['klit_bracing'] = $rootScope.total_quotation['base_plate']['count'];
	// 	temp_nut_boll['key_beam'] = $rootScope.total_quotation['beam']['count'];
	// 	temp_nut_boll['revet_beam'] = $rootScope.total_quotation['beam']['count'];

	// 	$rootScope.total_quotation['nutboll'] = temp_nut_boll;	
	// }

	// $scope.makeQuotationLogic();
});

makeQuotation.filter('removeUnderscores_capitalize', [function() {
return function(string) {
    if (!angular.isString(string)) {
        return string;
    }
    return string.replace(/[/_/]/g, ' ').substring(0,1).toUpperCase() + string.replace(/[/_/]/g, ' ').substr(1).toLowerCase();
 };
}])