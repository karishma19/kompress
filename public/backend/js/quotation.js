var makeQuotation = angular.module('makeQuotation',['ui.bootstrap'],function($interpolateProvider){ 
	$interpolateProvider.startSymbol('<%'); 
	$interpolateProvider.endSymbol('%>'); 
}); 


var ModalInstanceCtrl = function ($rootScope,$scope, $modalInstance, items) {

	$scope.items = $rootScope.total_quotation;

	$scope.ok = function () {
		$modalInstance.close($scope.selected);
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
};

makeQuotation.controller('makeQuotationController', function($rootScope,$scope,$http,$modal){
	$scope.quotation_parameter = {
		main_unit : 1,
	};

	// $scope.open = function () {
	// 	$.ajax({
	// 		type: "POST",
	// 		url: store_route,
	// 		data: { _token : token,quotation_parameter : $scope.quotation_parameter },
	// 		dataType: "application/json",
	// 		success: function(resultData){
	// 		}
	// 	});
	// };

	$rootScope.total_quotation = {};
	// $rootScope.total_multi_quotation = {};
	
	var base_plate = total_product['base_plate'];
	var beam = total_product['beam'];
	var bracing = total_product['bracing'];
	var bracing_length = total_product['bracing_length'];
	var connector = total_product['connector'];
	var diagonal_qty = total_product['diagonal_qty'];

	$scope.range = function(min, max, step) {
	    step = step || 1;
	    var input = [];
	    for (var i = min; i <= max; i += step) {
	        input.push(i);
	    }
	    return input;
	};

	$scope.checkedLoad = function(single_unit){
		if ($scope.quotation_parameter[single_unit] != undefined) {
			$scope.quotation_parameter[single_unit]['same_level'] = true;
		}
	}

	$scope.calculateTotalVolume = function(l,w,h,n,we){
		return (((l * w * h) * n) * we).toFixed(2);
	}

	//count parameter
	

	//change parameter on change of input parameter
	
	$scope.makeQuotationLogic = function(){
		
		$scope.main_unit = $scope.quotation_parameter['main_unit'];
		// $scope.add_on_unit = $scope.quotation_parameter['add_on_unit'];
		// $scope.levels = $scope.quotation_parameter['levels'];
		// $scope.height = $scope.quotation_parameter['height'];
		// $scope.width = $scope.quotation_parameter['width'];

		$scope.countUpright();
		$scope.basePlate();
		$scope.horizontalbracing();
		$scope.countCrossBracing();
		$scope.countBeam();

		$('#quotation_parameter').val(JSON.stringify($scope.quotation_parameter));
		$scope.nutBoll();
	}

	//count upright
	$scope.countUpright = function(){
		//upright
		temp_upright = {};
		temp_upright_count = 0;

		angular.forEach($scope.quotation_parameter,function(v,k){
			if (v.add_on_unit != undefined) {
				temp_upright_count = temp_upright_count + v.add_on_unit;

				temp_in_upright = {};
				temp_in_upright['count'] = 4 + (v.add_on_unit * 2);

				$scope.quotation_parameter[k]['upright'] = temp_in_upright;
			}
		});

		temp_upright['count'] = ($scope.main_unit * 4) + (temp_upright_count * 2);

		$rootScope.total_quotation['upright'] = temp_upright;
	}

	// base plate
	$scope.basePlate = function(){

		temp_baseplate = {};

		angular.forEach($scope.quotation_parameter,function(v,k){
			if (v.add_on_unit != undefined) {
				temp_in_baseplate = {};
				temp_in_baseplate['count'] = v['upright']['count'];
				$scope.quotation_parameter[k]['base_plate'] = temp_in_baseplate;
			}
		});

		temp_baseplate['count'] = $rootScope.total_quotation['upright']['count'];
		$rootScope.total_quotation['base_plate'] = temp_baseplate;
	}

	$scope.horizontalbracing = function(){
		//bracing
		temp_horizontal_bracing = {};
		temp_horizontal_bracing['count'] = $rootScope.total_quotation['upright']['count'];

		angular.forEach($scope.quotation_parameter,function(v,k){
			if (v.add_on_unit != undefined) {
				temp_in_horizontal_bracing = {};
				temp_in_horizontal_bracing['count'] = v['upright']['count'];
				$scope.quotation_parameter[k]['horizontal_bracing'] = temp_in_horizontal_bracing;
			}
		});
		
		$rootScope.total_quotation['horizontal_bracing'] = temp_horizontal_bracing;
	}

	$scope.checkLength = function(check_array,check_value,set_value){
		var keepGoing = true;

		var qty = 0;

		if (check_value != undefined && keepGoing == true) {
			angular.forEach(check_array,function(v,k){
				var previous_check = check_array[k-1];
				
				if (previous_check == undefined ) {
					previous_check = 0;
				}else{
					previous_check = check_array[k-1]['length'] + 1;
				}

				if (check_value <= v['length'] && check_value >= previous_check) {
					keepGoing = false;
					qty = v.qty;
				}
			})
		}

		return qty;
	}

	//count cross bracing
	$scope.countCrossBracing = function(){

		temp_cross_bracing = {};
		temp_cross_bracing_count = 0;

		angular.forEach($scope.quotation_parameter,function(v,k){
			if (v.height != undefined) {
				temp_in_cross_bracing = {};
				var count = 0;
				count = ($scope.checkLength(diagonal_qty,v.height) * ($scope.quotation_parameter[k]['upright']['count'] / 2));
				temp_in_cross_bracing['count'] = count;
				temp_cross_bracing_count = temp_cross_bracing_count + count;

				$scope.quotation_parameter[k]['cross_bracing'] = temp_in_cross_bracing;
			}
		});

		temp_cross_bracing['count'] = temp_cross_bracing_count;
		console.log($scope.quotation_parameter);
		$rootScope.total_quotation['cross_bracing'] = temp_cross_bracing;
	}

	function add() {
		return $('.add').toArray().reduce(function(sum,element) {
			return sum + Number(element.value);
		}, 0);
	}

	//count beam
	$scope.countBeam = function(){

		temp_beam = {};
		temp_beam_count = 0;
		temp_beam_weight_count = 0;

		angular.forEach($scope.quotation_parameter,function(v,k){
			temp_in_beam = {};

			if (v.same_level == false) {
				var total_single_beam = 1 + v.add_on_unit;

				temp_in_beam['count'] = (1 + v.add_on_unit) * (v.levels * 2);
				temp_beam_count = temp_beam_count + (1 + v.add_on_unit) * (v.levels * 2); 
			}
			if (v['width'] != undefined) {
				angular.forEach(v.load_lavel,function(iv,ik){
					
					var check_beam = [];
					angular.forEach(beam,function(vm,km){
						if (v['width'] <= km){
							check_beam.push(vm);
						};
					});

					check_beam = _.first(check_beam);
					check_beam = _.sortBy(check_beam,'load_bearing_capacity').reverse();

					var main_beam = _.map(check_beam,function(vn){
						if (vn.load_bearing_capacity >= iv.load) {
							temp_in_beam[ik] = {};
							temp_in_beam[ik]['value'] = iv;
							temp_in_beam[ik]['weight'] = parseFloat($scope.calculateTotalVolume(vn.height,vn.depth,vn.min_thickness,total_single_beam,vn.nett_weight));
							temp_beam_weight_count = temp_beam_weight_count + parseFloat($scope.calculateTotalVolume(vn.height,vn.depth,vn.min_thickness,total_single_beam,vn.nett_weight));
							temp_in_beam[ik]['beam_type'] = vn;

							return;
						}
					})
				});
			}

			$scope.quotation_parameter[k]['beam'] = temp_in_beam;
		})

		

		temp_beam['count']  = temp_beam_count;
		temp_beam['weight'] = temp_beam_weight_count;
		$rootScope.total_quotation['beam'] = temp_beam;
	};

	$scope.nutBoll = function(){
		var beam_count = $rootScope.total_quotation['beam']['count'];
		var upright_count = $rootScope.total_quotation['upright']['count'];

		temp_nut_boll = {};

		temp_nut_boll['base_to_upright'] = upright_count * 4;
		temp_nut_boll['bracing_to_upright'] = ($rootScope.total_quotation['cross_bracing']['count'] / 2) +  ($rootScope.total_quotation['horizontal_bracing']['count'] / 2) + 2;
		temp_nut_boll['anchor_fasteners'] = $rootScope.total_quotation['base_plate']['count'];
		temp_nut_boll['klit_bracing'] = $rootScope.total_quotation['base_plate']['count'];
		temp_nut_boll['key_beam'] = $rootScope.total_quotation['beam']['count'];
		temp_nut_boll['revet_beam'] = $rootScope.total_quotation['beam']['count'];

		$rootScope.total_quotation['nutboll'] = temp_nut_boll;	
	}

	$scope.makeQuotationLogic();
});

makeQuotation.filter('removeUnderscores_capitalize', [function() {
return function(string) {
    if (!angular.isString(string)) {
        return string;
    }
    return string.replace(/[/_/]/g, ' ').substring(0,1).toUpperCase() + string.replace(/[/_/]/g, ' ').substr(1).toLowerCase();
 };
}])